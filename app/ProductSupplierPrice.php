<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSupplierPrice extends Model
{
	//
	protected $fillable = [
		'product_id',
		'supplier_id',
		'sup_price',
	];
	public function product()
  {
   	return $this->belongsTo('App\Product','product_id');
  }
	public function product_supplier()
  {
   	return $this->belongsTo('App\ProductSupplier','supplier_id');
  }
}
