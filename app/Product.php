<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

define('STORAGE_PRODUCT', storage_path() . '/app/public/products');
if(!is_dir(STORAGE_PRODUCT)){
	mkdir(STORAGE_PRODUCT,0777,true);
}

class Product extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'prod_name',
		'prod_barcode',
		'prod_price',
		'prod_desc',
		'prod_stock',
		'prod_stock_no_unit',
	];
	protected $attributes = [];

	//Make it available in the json response
	//protected $appends = ['suppliers_prices'];//DEPRECATED

	//implement the attribute
	public function getSuppliersPricesAttribute()
	{
	 	return $this->get_suppliers_with_prices();
	}
  public function prices()
  {
    return $this->hasMany('App\ProductSupplierPrice','product_id');
  }
  public function acquisitions_by_barcode()
  {
    return $this->hasMany('App\Acquisition','prod_barcode','prod_barcode');
  }
  public function total_acquisitions_by_barcode()
  {
    return $this->acquisitions_by_barcode->count();
  }
  public function &acquisitions_by_barcode_to_calculate_purchase_price()
  {
    $records = $this->hasMany('App\Acquisition','prod_barcode','prod_barcode')
    				->orderBy('bought_at','desc')
    				->select(['id','prod_unit_price','prod_quantity']);

    return $records;
  }
  public function &acquisitions_by_barcode_to_calculate_purchase_price_order_by_bought_asc()
  {
    $records = $this->hasMany('App\Acquisition','prod_barcode','prod_barcode')
    				->orderBy('bought_at','asc');

    return $records;
  }
  public function get_suppliers_with_prices(){
  	$suppliers = ProductSupplier::all();
		$suppliers_prices_values = array_map(function($arr){
			return [
				'supplier_id' => $arr['supplier_id'],
				'supplier_price' => $arr['sup_price'],
			];
		}, ProductSupplierPrice::where('product_id', $this->id)->get(['supplier_id','sup_price'])->toArray());
		
		$result = [];
		foreach ( $suppliers as $supplier ) {
			$found = false;
			foreach ( $suppliers_prices_values as $sup_price_val ) {
				if( $sup_price_val['supplier_id'] == $supplier->id ){
					$price = $sup_price_val['supplier_price'];
					$found = true;
					break;
				}
			}
			if(!$found)$price = '0.0';
			$result[] = [
				'supplier' => $supplier,
				'price' => $price,
			];
		}

		return $result;

  }

  public function add_to_stock($amount, $by_weight = true){

  	if(!$by_weight){
  		$this->prod_stock += $amount;
  	}else{
  		$val2 = $this->prod_stock * 1000.0 + $this->prod_stock_no_unit + $amount;
			$val3 = floor( $val2 / 1000.0 );
			$remaining_quantity = $val2 - $val3 * 1000.0;
			$this->prod_stock = $val3;
			$this->prod_stock_no_unit = $remaining_quantity;
  	}

  }

  public static function import_from_csv(Request $request)
  {
  	$rows = [];
		$file = $request->file('csv_file');
		$contents = file_get_contents($file);
		$fila = 1;
		\Log::info("class = {get_class($file)}");
		if($file instanceof \Illuminate\Http\UploadedFile){
			if (($gestor = fopen($file->path(), "r")) !== FALSE) {
			 	while (($datos = fgetcsv($gestor, 3000, ",")) !== FALSE) {
					if(is_numeric(@$datos[1])){
						$rows[] = $datos;
					}
					$fila++;
				}
				fclose($gestor);
			}
		}
		try {
			\DB::transaction(function() use($rows){
				Product::truncate();
				foreach ($rows as $row) {
					$product_data = [
						'prod_name' => $row[2],
						'prod_barcode' => $row[1],
						'prod_price' => $row[6],
						'prod_stock' => ( is_numeric($row[3]) ? (float)$row[3] * ( is_numeric($row[4]) ? (float)$row[4] : 1 ) : 0 ) ,
						'prod_desc' => '',
					];
					(new Product($product_data))->save();
				}
			});			
		} catch (\Exception $e) {
			\Log::info($e->getMessage());
		}
		return $rows;
  }

  public function avg_price_from_acquisitions_discounting_stock(){

  	$records = $this->acquisitions_by_barcode_to_calculate_purchase_price;
  	$total_records = $records->count();
  	$sum_quantity = 0;
  	$sum_prod_prices = 0;
  	$total_records_iterated = 0;
  	if($total_records > 0){
	  	if($this->prod_stock>0){

		  	foreach ($records->all() as &$record) {

					$sum_quantity += $record->prod_quantity;
					$total_records_iterated += 1;
					$sum_prod_prices += (float)$record->prod_unit_price;

					if($sum_quantity >= $this->prod_stock){
						break;
					}

		  	}

		  	if($total_records_iterated > 0){
		  		return $sum_prod_prices / $total_records_iterated;
		  	}else{
		  		return null;
		  	}

	  	}else{
	  		return $records[0]->prod_unit_price;
	  	}
  	}{
  		return null;
  	}

  	/*
  	select 
  		prod_price 
  	from 
  		acquisitions 
  	where prod_barcode = $this->prod_barcode
  	have  
  		sum(prod_stock) $this->prod_stock
  	*/
  }

}
