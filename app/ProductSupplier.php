<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSupplier extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'sup_name',
		'sup_desc',
		'document_number',
		'document_type',
	];
  public function prices()
  {
    return $this->hasMany('App\ProductSupplierPrice','supplier_id');
  }
  public function acquisitions0()
  {
    return $this->hasMany('App\ProductSupplierAcquisition','supplier_id');
  }
  public function acquisitions()
  {
    return $this->hasMany('App\Acquisition','supplier_id');
  }

  public static function get_document_type_from_document_number($document_number){

    $document_number = trim(\preg_replace('/\s+/',' ',$document_number));
    if( \preg_match('/^20/',$document_number) === 1 && \strlen($document_number) == 11 ){
      $document_type = "RUC";
    }else if( \preg_match('/^10/',$document_number) === 1 && \strlen($document_number) == 11 ){
      $document_type = "RUC NATURAL";
    }else{
      $document_type = "DNI";
    }
    return $document_type;

  }

  public static function create_from_doc_name($document_number, $name){
    $faker = \Faker\Factory::create();

    $name = \strtoupper(trim(\preg_replace('/\s+/',' ',$name)));
    $document_number = \trim(\preg_replace('/\s+/',' ',$document_number));
    if(
      !\is_numeric($document_number) || 
      \preg_match('/^0+$/',$document_number)===1
    ){
      $document_number = $faker->numberBetween(10000000000,20000000000);
    }

    $record = null;

    if(!$record){
      $record = ProductSupplier::where('sup_name',$name)->first();
    }
    if(!$record){
      $record = ProductSupplier::where('document_number',$document_number)->first();
    }
    if(!$record){
      $record = ProductSupplier::where([
        ['sup_name',$name],
        ['document_number',$document_number],
      ])->first();
    }

		if(!$record){

			$record = new ProductSupplier;
			$record->sup_name = $name;
			$record->sup_desc = "";
			$record->document_number = $document_number;
			$record->document_type = self::get_document_type_from_document_number($document_number);
      $record->save();
      
		}
    return $record;
    
  }
}
