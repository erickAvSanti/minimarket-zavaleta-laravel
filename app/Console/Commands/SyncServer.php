<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use \App\Helpers\SyncServerHelper;

class SyncServer extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'sync:server';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Sync digitalocean server';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public CONST IGNORE_TABLES = [
		'users',
		'sync_server_tokens',
		'password_resets',
	];
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		//

		$ignored_tables = "";
		foreach(self::IGNORE_TABLES as $table_name){
			$ignored_tables .=" --ignore-table=".env('DB_DATABASE').".$table_name ";
		}

		\Log::info("begin request backup");
		$directory = storage_path('app/database_backups_to_upload/');
		if(!is_dir($directory)){
			mkdir($directory,0777);
		}
		$filename = env('DB_DATABASE')."_".\Carbon\Carbon::now()->format('Y_m_d__H_i_s').".sql";
		$file_path = $directory . $filename;
		SyncServerHelper::make_a_backup_for_file_path($file_path, $ignored_tables);
		
			
		if(file_exists($file_path)){

			$url = env('SYNC_DATABASE_SERVER_URL');
			$token = env('SYNC_DATABASE_SERVER_TOKEN');

			\Log::info("url = $url");
			\Log::info("token = $token");
			$fileContent = file_get_contents($file_path);
			/*
			$response = Http::withHeaders([
			  'X-SYNC-TOKEN' => $token,
			])->timeout(180)->attach(
				'remote_file',
				file_get_contents($file_path),
				$filename
			)->post($url);
			*/
			$response = Http::withHeaders([
				'X-SYNC-TOKEN' => $token,
			])->timeout(180)->attach(
				'remote_file',
				$fileContent,
				$filename
			)->post($url,[
				'name' => NULL,
				'contents' => NULL,
			]);
			\Log::info($response);
			if($response->successful()){
				\Log::info("ok");
			}else if($response->failed()){
				\Log::info("fail");
			}else{
				\Log::info("error");
			}
		}
		\Log::info("end request backup");

	}
}
