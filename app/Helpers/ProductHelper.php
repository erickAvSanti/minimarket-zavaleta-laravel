<?php
namespace App\Helpers;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \Carbon\Carbon;
use \App\Product;
use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
use Illuminate\Http\Request;
class ProductHelper{

	public static function excel_export_by_filter(Request $request)
	{

		$records = new Product;
		
		$order_by = $request->input('order_by') ?? '';
		$order = $request->input('order') ?? '';
		if(!empty($request->input('query'))){
			$records =$records->where(function ($query) use ($request) {
               $query->where('prod_name','LIKE',"%".$request->input('query')."%")
               ->orWhere('prod_barcode','LIKE',"%".$request->input('query')."%");
           });
		}
		if(!empty($order_by)){
			$records = $records->orderBy($order_by,$order);
		}

		if(isset($request->show_deleted)){
			$records = $records->onlyTrashed();
		}
		if($request->show_who_have_acquisitions === '1'){
			$records = $records->whereHas('acquisitions_by_barcode');
		}
		if($request->show_who_have_acquisitions === '2'){
			$records = $records->doesntHave('acquisitions_by_barcode');
		}

		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$rows[] = [
			'Código de barra',
			'Producto',
			'Precio de venta por unidad/kilo/litro',
			'Stock',
			'Eliminado ?',
			'Datos adicionales'
		];
		foreach ($records->get() as $kk => &$record) {
			$rows[] = [
				$record->prod_barcode,
				$record->prod_name,
				$record->prod_price,
				$record->prod_stock,
				( $record->deleted_at ? 'Eliminado' : '' ),
				$record->prod_desc,
			];
		}

		$sheet->getColumnDimension('A')->setWidth(20);
		$sheet->getColumnDimension('B')->setWidth(35);
		$sheet->getColumnDimension('C')->setWidth(19);
		$sheet->getColumnDimension('E')->setWidth(19);
		$sheet->getColumnDimension('F')->setWidth(30);

		self::setSheetWrapText($sheet,'C1');

		self::setSheetVerticalAlignment($sheet,'A1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'B1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'C1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'D1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'E1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'F1',Alignment::VERTICAL_CENTER);

		self::setSheetHorizontalAlignment($sheet,'A1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'B1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'C1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'D1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'E1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'F1',Alignment::HORIZONTAL_CENTER);
		$sheet->getRowDimension('1')->setRowHeight(30);

		$sheet->fromArray($rows,NULL, 'A1');
		$writer = new Xlsx($spreadsheet);
		$filename = STORAGE_PRODUCT . "/filtro_productos_" . \Carbon\Carbon::now()->setTimezone(env('APP_TIMEZONE'))->format('d_m_Y_h_i_s_a') . '_' . \Str::random(20) . ".xlsx";
		$writer->save($filename);
		return $filename;
	}

	public static function excel_export_all($to_url = false)
	{
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();

		$rows[] = [
			'Código de barra',
			'Producto',
			'Precio de venta por unidad/kilo/litro',
			'Stock',
			'Eliminado ?',
			'Datos adicionales'
		];
		foreach (Product::withTrashed()->get() as $kk => &$record) {
			$rows[] = [
				$record->prod_barcode,
				$record->prod_name,
				$record->prod_price,
				$record->prod_stock,
				( $record->deleted_at ? 'Eliminado' : '' ),
				$record->prod_desc,
			];
		}

		$sheet->getColumnDimension('A')->setWidth(20);
		$sheet->getColumnDimension('B')->setWidth(35);
		$sheet->getColumnDimension('C')->setWidth(19);
		$sheet->getColumnDimension('E')->setWidth(19);
		$sheet->getColumnDimension('F')->setWidth(30);

		self::setSheetWrapText($sheet,'C1');

		self::setSheetVerticalAlignment($sheet,'A1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'B1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'C1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'D1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'E1',Alignment::VERTICAL_CENTER);
		self::setSheetVerticalAlignment($sheet,'F1',Alignment::VERTICAL_CENTER);

		self::setSheetHorizontalAlignment($sheet,'A1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'B1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'C1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'D1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'E1',Alignment::HORIZONTAL_CENTER);
		self::setSheetHorizontalAlignment($sheet,'F1',Alignment::HORIZONTAL_CENTER);
		$sheet->getRowDimension('1')->setRowHeight(30);

		$sheet->fromArray($rows,NULL, 'A1');
		$writer = new Xlsx($spreadsheet);
		$filename = STORAGE_PRODUCT . "/full_" . \Carbon\Carbon::now()->setTimezone(env('APP_TIMEZONE'))->format('d_m_Y_h_i_s_a') . '_' . \Str::random(20) . ".xlsx";
		$writer->save($filename);
		if($to_url){
			return self::toPublicExcelReportLink($filename);
		}else{
			return $filename;
		}
	}

	public static function toPublicExcelReportLink($filename)
	{
		return preg_replace('/.+\/(products.+)$/',"storage/$1",$filename);
	}

	public static function setSheetBold( &$sheet, $cell, $flag = true)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->setBold($flag);
	}

	public static function setSheetColor( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->getColor()
			->setARGB($color);
	}

	public static function setSheetFill( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFill()
			->setFillType(Fill::FILL_SOLID)
			->getStartColor()->setARGB($color);
	}

	public static function setSheetHorizontalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setHorizontal($align);
	}

	public static function setSheetVerticalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setVertical($align);
	}

	public static function setSheetWrapText(&$sheet, $cell, $wrap = true)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setWrapText($wrap);
	}

}