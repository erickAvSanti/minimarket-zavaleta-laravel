<?php
namespace App\Helpers;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \Carbon\Carbon;
use \App\MotorcyclePart;
use \App\MotorcyclePartsSupplierItem;
use \App\MotorcyclePartsSupplierItemOnSale;
use \App\MotorcycleModel;
use \App\Acquisition;
use \App\AcquisitionMeasurementUnit;
use \App\ProductSupplier;
use \App\Product;
use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
class ImportExcelHelper{
    public static function import_data_from_unregistered_acquisitions(){

		$inputFileName = __DIR__ . '/../../storage/app/SISTEMA MINI.xlsx';
		$spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($inputFileName);
		
		//TVS
		$worksheet = $spreadsheet->setActiveSheetIndex(0);
        $rows = $worksheet->toArray();
        foreach($rows AS &$row){
            foreach($row AS &$value){
                if(\is_string($value)){
                    $value = trim(\preg_replace('/\s+/',' ',$value));
                }
            }
        }
        //\Log::info($rows);
		self::import_data_from_unregistered_acquisitions_process_worksheet($rows);
    }
    public static function import_data_from_unregistered_acquisitions_process_worksheet(&$rows){

        $counter_test = 0;
        $counter_max_test = 10;
        foreach($rows AS &$row){
            /*
            if($counter_test > $counter_max_test)break;
            $counter_test++;
            */
            \Log::info($row);
            if(!isset($row[5])){
                \Log::info("Fecha de compra no definido");
                continue;   
            }
            if(!preg_match('/^\d{1,2}\/\d{1,2}\/\d{4}$/',$row[5])){
                \Log::info("Fecha de compra mal estructurado '".$row[5]."'");
                continue;   
            }
            try{

                $business_document = $row[8];
                $business_name = $row[9];
                $measurement_unit_name = $row[12];

                \Log::info("current bought_at = " . $row[5]);
                $bought_at = Carbon::createFromFormat('m/d/Y',$row[5])->toDateString();
                $bought_at2 = Carbon::createFromFormat('d/m/Y',$row[5])->toDateString();
                \Log::info("current bought_at after carbon transform = " . $bought_at);

                $measurement_unit   = AcquisitionMeasurementUnit::create_from_name( $measurement_unit_name );
                $supplier           = ProductSupplier::create_from_doc_name( $business_document, $business_name );

                if(!$measurement_unit){
                    \Log::info("measurement_unit not found");
                    \Log::info($measurement_unit_name);
                    continue;
                }
                if(!$supplier){
                    \Log::info("supplier not found");
                    \Log::info($supplier);
                    continue;
                }

                $product = Product::where('prod_barcode',$row[2])->first();

                Acquisition::where([
                    ['prod_barcode',$row[2]],
                    ['document_number',$row[7]],
                    ['bought_at',$bought_at],
                ])->forceDelete();

                Acquisition::where([
                    ['prod_barcode',$row[2]],
                    ['document_number',$row[7]],
                    ['bought_at',$bought_at2],
                ])->forceDelete();

                Acquisition::where([
                    ['prod_name',$row[10]],
                    ['bought_at',$bought_at],
                ])->forceDelete();

                $adquisition = null;

                if( !$adquisition ){

                    $where_acq = [
                        ['prod_name',$row[10]],
                        ['bought_at',$bought_at],
                    ] ;
                    \Log::info("buscando para1:");
                    \Log::info($where_acq);
                    $acquisition = Acquisition::where($where_acq)->first();

                }

                if( !$adquisition ){

                    $where_acq = [
                        ['prod_barcode',$row[2]],
                        ['document_number',$row[7]],
                        ['bought_at',$bought_at],
                    ];
                    \Log::info("buscando para2:");
                    \Log::info($where_acq);
                    $acquisition = Acquisition::where($where_acq)->first();

                }

                if(!$acquisition){

                    $acquisition = new Acquisition;
                    $acquisition->prod_barcode      = $row[2];
                    $acquisition->prod_name         = $row[10];
                    $acquisition->prod_group        = $row[11];
                    $acquisition->prod_quantity     = $row[13] ?? 0;
                    $acquisition->prod_unit_price   = $row[14] ?? 0;
                    $acquisition->igv_type          = $row[15] == 2 ? 2 : 0;

                    $acquisition->document_type     = $row[6];
                    $acquisition->document_number   = $row[7];

                    $acquisition->measurement_unit_id       = $measurement_unit->id;
                    $acquisition->supplier_id               = $supplier->id;
                    $acquisition->total_price               = preg_replace('/,/','',$row[16]);
                    $acquisition->bought_at                 = $bought_at;
                    $acquisition->prod_unit_price_for_sale  = ( $product ? $product->prod_price : $acquisition->prod_unit_price );

                    $acquisition->save();

                }else{
                    \Log::info("acquisition exists");
                    \Log::info($acquisition);
                    $acquisition->bought_at = $bought_at;
                    $acquisition->prod_quantity     = $row[13] ?? 0;
                    $acquisition->prod_unit_price   = $row[14] ?? 0;
                    $acquisition->save();
                }
                
                
                /*
                
                'prod_unit_price_for_sale',
                */


            }catch(\Exception $e){
                \Log::info($e->getMessage());
                \Log::info($e->getTraceAsString());
            }

        }

    }
}