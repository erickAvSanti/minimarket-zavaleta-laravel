<?php
namespace App\Helpers;
use \Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Console\Commands\SyncServer;
use Illuminate\Support\Facades\Schema;
class SyncServerHelper{
    public static function bind_uploaded_file_to_db($file_path)
    {
        if(file_exists($file_path) && !app()->environment('local'))
        {
            
            self::make_a_local_backup_before_restore_from_clients();
            $tables_to_drop = [];
            foreach(\DB::select('show tables') AS &$table){
                foreach($table AS $k => $v){
                    $tables_to_drop[] = $v;
                }
            }
            foreach($tables_to_drop as $table_name)
            {
                if( !in_array( $table_name,SyncServer::IGNORE_TABLES ) ){
                    Schema::dropIfExists($table_name);
                }
            }
            $restore_db_command = "mysql -u".env('DB_USERNAME').( !empty(env('DB_PASSWORD')) ? " -p".env('DB_PASSWORD') : '')." ".env('DB_DATABASE')." < ".$file_path;
            \Log::info("restoring DB");
            \Log::info("Command $restore_db_command");
            $res = shell_exec($restore_db_command);
            \Log::info("$res");
            \Log::info("finish");
		
        }
    }
    public static function make_a_local_backup_before_restore_from_clients()
    {
        $directory = storage_path('app/database_backups_before_restore_from_clients/');
        if(!is_dir($directory)){
            mkdir($directory,0777);
        }
        \App\Helpers\MariaDBHelper::make_a_full_db_backup_to_directory($directory);
            
    }
    public static function make_a_backup_for_file_path($file_path = null, $ignored_tables = ""){
        if(!$file_path){
            
            $directory = storage_path('app/temporal_database_backups_for_testing/');
            if(!is_dir($directory)){
                mkdir($directory,0777);
            }
            $filename = env('DB_DATABASE')."_".\Carbon\Carbon::now()->format('Y_m_d__H_i_s').".sql";
            $file_path = $directory . $filename;
        }
        shell_exec("mysqldump -u".env('DB_USERNAME').( !empty(env('DB_PASSWORD')) ? " -p".env('DB_PASSWORD') : '')." ".env('DB_DATABASE')." $ignored_tables --skip-tz-utc > ".$file_path);
    }
}