<?php
namespace App\Helpers;
use \Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
class StorageHelper{
    public static function make_accessible_directory($directory){
        if(!is_dir($directory)){
            mkdir($directory,0777);
        }
        return $directory;
    }
    public static function remove_files_from_directory_who_are_older_than($directory, $older_than_days){
        
        $end_char_directory = substr($directory,-1);
        if($end_char_directory !== '/')$directory .= '/';
        
        if(\is_dir($directory)){
            $files = scandir($directory);
            foreach($files AS $file){
                $file_path = $directory . $file;
                if(\is_file($file_path)){
                    $timestamp_now = \Carbon\Carbon::now()->timestamp;
                    $timestamp_file = \filemtime($file_path);
                    if( ( $timestamp_now - $timestamp_file )/60/60/24.0 > $older_than_days ){
                        \unlink($file_path);
                    }
                }
            }
        }
    }
}