<?php 
namespace App\Helpers;
use \App\Acquisition;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \Carbon\Carbon;
use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
use \PhpOffice\PhpSpreadsheet\Style\NumberFormat;
class AcquisitionHelper{
	
	public static function get_acquisitions(){
		$rows = [];
		foreach ( Acquisition::all() as &$record ) {
			$row = $record->toArray();
			$row['bought_at'] = $record->bought_at ? $record->bought_at : null;
			$row['supplier'] = $record->product_supplier;
			$row['measurement_unit'] = $record->measurement_unit;
			$rows[] = $row;
		}
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Principal');
		//$sheet->setCellValue('A1', 'Hello World !');
		$sheet_rows[] = [
			'Mes',
			'Fecha',
			'Tipo de Documento',
			'#Documento',
			'RUC',
			'Razón Social',
			'Producto',
			'Unidad de medida',
			'Grupo',
			'Cantidad',
			'Precio unitario',
			'Grab. IGV',
			'Total',
		];
		foreach ($rows as &$row) {
			# code...
			$supplier = @$row['supplier'] ?? null;
			$measurement_unit = @$row['measurement_unit'] ?? null;
			$sheet_rows[] = [
				'',
				'',
				$row['document_type'],
				$row['document_number'],
				($supplier ? $supplier['document_number'] : ''),
				($supplier ? $supplier['sup_name'] : ''),
				$row['prod_name'],
				($measurement_unit ? $measurement_unit['mu_name'] : ''),
				$row['prod_group'],
				$row['prod_quantity'],
				$row['prod_unit_price'],
				$row['igv_type'],
				$row['total_price'],
			];
		}
		for($i = 'B'; $i <= 'N'; $i++){
			self::setSheetBorderColor($sheet,"{$i}3",'FF000000');
			self::setSheetHorizontalAlignment(
					$sheet, 
					"{$i}3", 
					Alignment::HORIZONTAL_CENTER);
			self::setSheetVerticalAlignment(
					$sheet, 
					"{$i}3", 
					Alignment::VERTICAL_CENTER);
			self::setSheetWrapText( $sheet, "{$i}3" );
			$sheet->getColumnDimension($i)->setWidth(25);
		}

		for($i = 'B'; $i <= 'N'; $i++){
			for($j = 3; $j < 3 + count($sheet_rows); $j++){
				self::setSheetBorderColor($sheet,"{$i}${j}",'FF000000');
				if($j == 3){
					self::setSheetBold($sheet,"{$i}3");
				}
			}
		}
		$sheet->fromArray($sheet_rows,NULL, 'B3');
		foreach ($rows as $key_row => &$value_row) {
			if(@$value_row['bought_at']){
				$sheet->setCellValue(
					"B". ( $key_row + 4 ), 
					\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(
						$value_row['bought_at']
					)
				);
				self::setSheetDateFormat(
					$sheet,
					"B" . ( $key_row + 4 ),
					NumberFormat::FORMAT_DATE_XLSX17
				);
				$sheet->setCellValue(
					"C". ( $key_row + 4 ), 
					\PhpOffice\PhpSpreadsheet\Shared\Date::PHPToExcel(
						$value_row['bought_at']
					)
				);
				self::setSheetDateFormat(
					$sheet,
					"C" . ( $key_row + 4 ),
					NumberFormat::FORMAT_DATE_DDMMYYYY
				);
			}
		}

		$writer = new Xlsx($spreadsheet);
		$filename = STORAGE_ACQUISITION_REPORT . "/reporte_diario_" . 
			\Carbon\Carbon::now()->setTimezone(env('APP_TIMEZONE'))
			->format('d_m_Y_h_i_s_a') . '_' . \Str::random(20) . ".xlsx";
		
		$writer->save($filename);
		
		return self::toPublicExcelReportLink($filename);
	
	}
	public static function toPublicExcelReportLink($filename)
	{
		return preg_replace('/.+\/(acquisitions.+)$/',"storage/$1",$filename);
	}

	public static function setSheetBold( &$sheet, $cell, $flag = true)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->setBold($flag);
	}

	public static function setSheetColor( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->getColor()
			->setARGB($color);
	}

	public static function setSheetFill( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFill()
			->setFillType(Fill::FILL_SOLID)
			->getStartColor()->setARGB($color);
	}

	public static function setSheetHorizontalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setHorizontal($align);
	}

	public static function setSheetVerticalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setVertical($align);
	}

	public static function setSheetWrapText(&$sheet, $cell, $wrap = true)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setWrapText($wrap);
	}

	public static function setSheetDateFormat(&$sheet, $cell, $format = NumberFormat::FORMAT_DATE_YYYYMMDDSLASH )
	{

		$sheet->getStyle($cell)
    ->getNumberFormat()
    ->setFormatCode($format);
	}

	public static function setSheetBorderColor(&$sheet, $cell, $color)
	{
		$borders = $sheet->getStyle($cell)
    ->getBorders();

		$borders->getTop()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getTop()->getColor()->setARGB($color);
    
		$borders->getBottom()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getBottom()->getColor()->setARGB($color);
    
		$borders->getLeft()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getLeft()->getColor()->setARGB($color);
    
		$borders->getRight()->setBorderStyle(
    	\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

		$borders->getRight()->getColor()->setARGB($color);

	}	
}