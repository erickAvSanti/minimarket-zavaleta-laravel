<?php
namespace App\Helpers;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use \Carbon\Carbon;
use \App\Product;
use \App\Sale;
use \PhpOffice\PhpSpreadsheet\Style\Fill;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
class SaleHelper{
	public static function get_prod_type_fullname($prod)
	{
		return @$prod['prod_by_weight'] == 'yes' ? self::get_prod_type_name($prod['prod_weight_type']) : 'c/u';
	}
	public static function get_prod_type_shortname($prod)
	{
		return @$prod['prod_by_weight']=='yes' ? $prod['prod_weight_type'] : ' u';
	}
	public static function get_prod_type_name($str,$master_unit = true)
	{
		if($str == 'g')
		{
			if($master_unit)return 'Kilogramo';
			else return 'Gramo';
		}
		if($str == 'ml')
		{
			if($master_unit)return 'Litro';
			else return 'Mililitro';
		}
		return '';
	}
	public static function get_sales_report($by, $request){
		if($by == 'sales_of_the_day'){
			$records = &Sale::get_all_from_today();
		}elseif($by == 'specific_dates'){
			$records = &Sale::get_from_dates(
					$request->from_date, 
					$request->to_date
				);
		}elseif($by == 'specific_ids'){
			$records = &Sale::get_from_ids(
					$request->from_id, 
					$request->to_id
				);
		}else{
			return null;
		}
		\Log::info($records);
		if(count($records)==0){
			return null;
		}
		$spreadsheet = new Spreadsheet();
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->setTitle('Principal');
		//$sheet->setCellValue('A1', 'Hello World !');
		$rows = array();
		$row_count = 1;
		if($by == 'specific_dates'){
			$rows[] = [];
			$rows[] = ["Fecha: " . $request->from_date . " - " . $request->to_date];
		
			$row_count += 2;
		}

		$product_ids_by_unit = [];
		$sold_products_by_unit = [];

		$product_ids_by_weight = [];
		$sold_products_by_weight = [];
		$total_sold = 0;
		$total_revenue_price = 0;
		foreach ($records as $key_record => &$record) 
		{
			$products = json_decode($record->json_products,true);
			$record->calc_total_values($products);
			$total_sold += $record->sale_total;
			$total_group_revenue_price = 0;
			$tmp_row = $row = [
				$record->format_sale_code(),
				'Precio de compra por unidad, kilo o litro',
				'Precio de venta por unidad, kilo o litro',
				'Precio calculado de venta',
				'Cantidad',
				'u/kg/g/l/ml',
				'Stock (Unid, Kg, Lt)',
				'Stock (mL, Gr)',
				'Precio calculado de compra',
				'Ganancia',
			];
			$rows[] = $row;
			self::setSheetFill($sheet,"A$row_count:J$row_count","FF96BF2A");
			self::setSheetWrapText($sheet,"A$row_count:J$row_count");
			self::setSheetAlignment($sheet, "A$row_count:J$row_count", Alignment::HORIZONTAL_CENTER);
			self::setSheetVerticalAlignment($sheet,"A$row_count:J$row_count",Alignment::VERTICAL_CENTER);

			$row_count ++;
			foreach ($products as $key_prod => &$product) 
			{
				$product['id'] = (int)$product['id'];
				$product_record = Product::find($product['id']);
				if( !isset($product_ids_by_unit[$product['id']]) )
				{
					if( @$product['prod_by_weight'] != 'yes' )
					{
						$product_ids_by_unit[$product['id']] = $product['prod_quantity'];
						$sold_products_by_unit[$product['id']] = $product['prod_name'];
					}else
					{
						$product_ids_by_weight[$product['id']] = $product['prod_quantity'];
						$sold_products_by_weight[$product['id']] = $product['prod_name'];
					}
				}else
				{
					if( @$product['prod_by_weight'] != 'yes' )
					{
						$product_ids_by_unit[$product['id']] += $product['prod_quantity'];
					}else
					{
						if(!isset($product_ids_by_weight[$product['id']])){
							$product_ids_by_weight[$product['id']] = $product['prod_quantity'];
						}else{
							$product_ids_by_weight[$product['id']] += $product['prod_quantity'];
						}
					}
				}
				$row = [];
				
				/*
				$row[] = strtoupper($product['prod_name']).( 
							@$product['prod_stock'] ? 
							" (stock = ". @$product['prod_stock'] . ( 
								@$product['prod_stock_no_unit'] ? 
								" + " . ( @$product['prod_stock_no_unit'] / 1000.0 ) : '' 
							) . ")" : '' 
						);
				*/
				
				$row[] = strtoupper($product['prod_name']).( isset($product['prod_barcode']) ? "\n".$product['prod_barcode'] : '');

				if(isset($product['prod_purchase_price']) && \is_numeric($product['prod_purchase_price'])){
					$purchase_price = $product['prod_purchase_price'];
				}else{
					$purchase_price = $product_record ? $product_record->avg_price_from_acquisitions_discounting_stock() : 0;
				}
				$row[] = ( is_numeric($purchase_price) && $purchase_price != 0 ? round($purchase_price,2) : "0" );//precio de compra
				$row[] = $product['prod_price'];//precio de venta
				$no_apply_discount = empty(@$product['discount_sale']) || @$product['discount_sale'] == 'no';
				//$row[] = $product['prod_by_weight'] == 'yes' ? (float)$product['prod_quantity'] * ( (float)$product['prod_price'] / 1000.0 ) : $product['prod_price'];
				
				$final_product_price_using_purchase_price = $product['prod_quantity'] * ( @$product['prod_by_weight'] == 'yes' ? ( $purchase_price / 1000.0 ) : $purchase_price );
				if($no_apply_discount){
					$final_product_price = $product['prod_quantity'] * ( @$product['prod_by_weight'] == 'yes' ? ( $product['prod_price'] / 1000.0 ) : $product['prod_price'] );
				}else{
					$final_product_price = (float)$product['discount_sale_price'];
				}
				$final_product_price_using_purchase_price = is_numeric($final_product_price_using_purchase_price) && $final_product_price_using_purchase_price != 0 ? round($final_product_price_using_purchase_price,2) : "0";
				if(  
					\is_numeric($final_product_price_using_purchase_price) && 
					\is_numeric($final_product_price) 
				){
					$revenue_price = $final_product_price - $final_product_price_using_purchase_price;
					$total_revenue_price += $revenue_price;
					$total_group_revenue_price += $revenue_price;
				}else{
					$revenue_price = 0;
				}
				$row[] = sprintf('%0.2f', $final_product_price);
				$row[] = $product['prod_quantity'];
				$row[] = self::get_prod_type_shortname($product);
				$row[] = @$product['prod_stock'];
				$row[] = @$product['prod_stock_no_unit'] / 1000.0;
				$row[] = $final_product_price_using_purchase_price;
				$row[] = round( $revenue_price ,2);
				$rows[] = $row;
				self::setSheetWrapText($sheet,"A$row_count");
				self::setSheetFill($sheet, "A$row_count:H$row_count", "FFF9FFCC");
				if(!$no_apply_discount){
					self::setSheetColor($sheet, "D$row_count", "FF0000FF");
					self::setSheetBold($sheet,"D$row_count");
				}
				self::setSheetFill($sheet, "I$row_count:J$row_count", "FF7FD0E4");
				if($final_product_price_using_purchase_price == 0){
					self::setSheetFill($sheet, "I$row_count", "FFAFA7F3");
					self::setSheetBold($sheet,"I$row_count");
				}else{
				}
				$row_count ++;
			}
			$rows[] = [
				$record->created_at->setTimeZone(env('APP_TIMEZONE'))->format('d/m/Y h:i:s a'),
				'',
				'Total',
				sprintf('%0.2f',(float)$record->sale_total),
				'',
				'',
				'',
				'',
				'',
				sprintf('%0.2f',$total_group_revenue_price),
			];

			self::setSheetAlignment($sheet, "C$row_count:D$row_count", Alignment::HORIZONTAL_RIGHT);
			self::setSheetBold($sheet, "C$row_count:D$row_count", true);
			self::setSheetColor($sheet, "C$row_count:D$row_count", "FFFF0000");
			self::setSheetFill($sheet, "C$row_count:D$row_count", "FFF2D05B");
			self::setSheetFill($sheet, "J$row_count", "FFF2D05B");
			self::setSheetColor($sheet, "J$row_count", "FFFF0000");
			self::setSheetBold($sheet, "J$row_count", true);
			
			$row_count ++;
			$rows[] = [];
			$row_count ++;
		}
		$rows[] = [
			'',
			'',
			'Venta Total S/.',
			sprintf("%0.2f",$total_sold),
			'',
			'',
			'',
			'',
			'',
			sprintf("%0.2f",$total_revenue_price),
		];
		self::setSheetAlignment($sheet, "C$row_count", Alignment::HORIZONTAL_RIGHT);
		self::setSheetBold($sheet, "C$row_count:D$row_count", true);
		self::setSheetColor($sheet, "C$row_count:D$row_count", "FFFF0000");
		self::setSheetFill($sheet, "C$row_count:D$row_count", "FFF2D05B");

		self::setSheetBold($sheet, "J$row_count", true);
		self::setSheetColor($sheet, "J$row_count", "FFFF0000");
		self::setSheetFill($sheet, "J$row_count", "FFF2D05B");
		$row_count ++;
		
		$sheet->getColumnDimension('A')->setWidth(35);
		$sheet->getColumnDimension('B')->setWidth(27);
		$sheet->getColumnDimension('C')->setWidth(27);
		$sheet->getColumnDimension('D')->setWidth(27);
		$sheet->getColumnDimension('E')->setWidth(27);
		$sheet->getColumnDimension('F')->setWidth(27);

		$row_count += 2;
		$index = $row_count - 1;
		$rows[] = [];
		$rows[] = ['POR UNIDAD/KILO/LITRO'];

		self::setSheetBold($sheet,"A$index",true);
		self::setSheetColor($sheet,"A$index","FFFF00FF");
		self::setSheetFill($sheet,"A$index","FF32C8E0");

		foreach ($sold_products_by_unit as $key_sold_prod => &$sold_product) 
		{
			$rows[] = [
				$sold_product,
				$product_ids_by_unit[$key_sold_prod],
			];
			$row_count ++;
		}
		$row_count += 2;
		$index = $row_count - 1;
		$rows[] = [""];
		$rows[] = ['POR PESO'];

		self::setSheetBold($sheet,"A$index",true);
		self::setSheetColor($sheet,"A$index","FFFF00FF");
		self::setSheetFill($sheet,"A$index","FF32C8E0");

		foreach ($sold_products_by_weight as $key_sold_prod => &$sold_product) 
		{
			$rows[] = [
				$sold_product,
				$product_ids_by_weight[$key_sold_prod],
			];
			$row_count ++;
		}
		$sheet->fromArray($rows,NULL, 'A1');

		//SECOND SHEET
		$spreadsheet->createSheet();
		// Zero based, so set the second tab as active sheet
		$spreadsheet->setActiveSheetIndex(1);
		$spreadsheet->getActiveSheet()->setTitle('Sumatoria');

		$rows = array();
		$rows[] = [
			'Código de venta',
			'Fecha y hora de venta',
			'Monto'
		];
		$sheet = $spreadsheet->getActiveSheet();
		$sheet->getColumnDimension('A')->setWidth(27);
		$sheet->getColumnDimension('B')->setWidth(35);
		$sheet->getColumnDimension('C')->setWidth(27);
		$row_count = 1;
		self::setSheetAlignment($sheet, "A$row_count:C$row_count", Alignment::HORIZONTAL_CENTER);
		self::setSheetBold($sheet,"A$row_count:C$row_count",true);
		self::setSheetColor($sheet,"A$row_count:C$row_count","FFFF00FF");
		self::setSheetFill($sheet,"A$row_count:C$row_count","FF32C8E0");
		foreach ($records as $key_record => &$record) 
		{
			$rows[] = [
				$record->format_sale_code(),
				$record->created_at->setTimeZone(env('APP_TIMEZONE'))->format('d/m/Y h:i:s a'),
				sprintf("%0.2f",$record->sale_total),
			];
			$row_count ++;
		}
		$sheet->fromArray($rows,NULL, 'A1');
		//set first sheet as active sheet
		$spreadsheet->setActiveSheetIndex(0);

		$writer = new Xlsx($spreadsheet);
		$filename = STORAGE_EXCEL_REPORT_DAILY . "/reporte_diario_" . \Carbon\Carbon::now()->setTimezone(env('APP_TIMEZONE'))->format('d_m_Y_h_i_s_a') . '_' . \Str::random(20) . ".xlsx";
		$writer->save($filename);
		return self::toPublicExcelReportLink($filename);
	}
	public static function toPublicExcelReportLink($filename)
	{
		return preg_replace('/.+\/(excel_reports.+)$/',"storage/$1",$filename);
	}

	public static function setSheetBold( &$sheet, $cell, $flag = true)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->setBold($flag);
	}

	public static function setSheetColor( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFont()
			->getColor()
			->setARGB($color);
	}

	public static function setSheetFill( &$sheet, $cell, $color)
	{
		return $sheet
			->getStyle($cell)
			->getFill()
			->setFillType(Fill::FILL_SOLID)
			->getStartColor()->setARGB($color);
	}

	public static function setSheetAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setHorizontal($align);
	}

	public static function setSheetVerticalAlignment(&$sheet, $cell, $align)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setVertical($align);
	}

	public static function setSheetWrapText(&$sheet, $cell, $wrap = true)
	{

		return $sheet
			->getStyle($cell)
  		->getAlignment()
  		->setWrapText($wrap);
	}

	public static function setup_total_price(){
		foreach (Sale::all() as &$sale) {
			$products = json_decode($sale->json_products,true);
			$sale->sale_total = Sale::calc_total_values2($products);
			$sale->update();
		}
	}

}