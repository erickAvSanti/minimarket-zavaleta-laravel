<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use \App\AcquisitionMeasurementUnit;
use Illuminate\Http\Request;
use \App\Helpers\AcquisitionHelper;
use Illuminate\Database\Eloquent\SoftDeletes;

define('STORAGE_ACQUISITION_REPORT', storage_path() . '/app/public/acquisitions');
define('STORAGE_ACQUISITION_REPORT_DAILY', STORAGE_ACQUISITION_REPORT . '/daily');
if(!is_dir(STORAGE_ACQUISITION_REPORT_DAILY)){
	mkdir(STORAGE_ACQUISITION_REPORT_DAILY,0711,true);
}
class Acquisition extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'bought_at',
		'supplier_id',
		'prod_name',
		'prod_barcode',
		'measurement_unit_id',
		'prod_group',
		'prod_quantity',
		'prod_unit_price',
		'prod_unit_price_for_sale',
		'igv_type',
		'total_price',
		'document_type',
		'document_number',
	];
	protected $casts = [
		'bought_at' => 'date',
	];

	public function product_supplier()
	{
		return $this->belongsTo('App\ProductSupplier','supplier_id');
	}
	public function measurement_unit()
	{
		return $this->belongsTo(
			'App\AcquisitionMeasurementUnit','measurement_unit_id');
	}
	public function product()
	{
		return $this->belongsTo(
			'App\Product','prod_barcode','prod_barcode');
	}
	public static function gen_excel_report_by(Request $request){
		$result = AcquisitionHelper::get_acquisitions();
		return $result;
	}
}
