<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyncServerToken extends Model
{
	//
	protected $fillable = [
		'sync_token',
	];
}
