<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		//
	}

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
		//
		Schema::defaultStringLength(191);
		\App\ProductSupplier::observe(
			\App\Observers\ProductSupplierObserver::class);

		\App\AcquisitionMeasurementUnit::observe(
			\App\Observers\AcquisitionMeasurementUnitObserver::class);
	}
}
