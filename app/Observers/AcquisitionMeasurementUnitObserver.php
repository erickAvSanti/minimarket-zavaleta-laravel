<?php

namespace App\Observers;

use App\AcquisitionMeasurementUnit;

class AcquisitionMeasurementUnitObserver
{
	/**
	 * Handle the acquisition measurement unit "created" event.
	 *
	 * @param  \App\AcquisitionMeasurementUnit  $acquisitionMeasurementUnit
	 * @return void
	 */
	public function created(AcquisitionMeasurementUnit $acquisitionMeasurementUnit)
	{
		//
	}

	/**
	 * Handle the acquisition measurement unit "updated" event.
	 *
	 * @param  \App\AcquisitionMeasurementUnit  $acquisitionMeasurementUnit
	 * @return void
	 */
	public function updated(AcquisitionMeasurementUnit $acquisitionMeasurementUnit)
	{
		//
	}

	/**
	 * Handle the acquisition measurement unit "deleted" event.
	 *
	 * @param  \App\AcquisitionMeasurementUnit  $acquisitionMeasurementUnit
	 * @return void
	 */
	public function deleted(AcquisitionMeasurementUnit $acquisitionMeasurementUnit)
	{
		//
	}
	public function deleting(AcquisitionMeasurementUnit $acquisitionMeasurementUnit)
	{
		//
		if($acquisitionMeasurementUnit->acquisitions->count()>0){
			throw new \Exception('Esta unidad de medida cuenta con adquisiciones asignadas');
		}
	}

	/**
	 * Handle the acquisition measurement unit "restored" event.
	 *
	 * @param  \App\AcquisitionMeasurementUnit  $acquisitionMeasurementUnit
	 * @return void
	 */
	public function restored(AcquisitionMeasurementUnit $acquisitionMeasurementUnit)
	{
		//
	}

	/**
	 * Handle the acquisition measurement unit "force deleted" event.
	 *
	 * @param  \App\AcquisitionMeasurementUnit  $acquisitionMeasurementUnit
	 * @return void
	 */
	public function forceDeleted(AcquisitionMeasurementUnit $acquisitionMeasurementUnit)
	{
		//
	}
}
