<?php

namespace App\Observers;

use App\ProductSupplier;

class ProductSupplierObserver
{
	/**
	 * Handle the product supplier "created" event.
	 *
	 * @param  \App\ProductSupplier  $productSupplier
	 * @return void
	 */
	public function created(ProductSupplier $productSupplier)
	{
		//
	}

	/**
	 * Handle the product supplier "updated" event.
	 *
	 * @param  \App\ProductSupplier  $productSupplier
	 * @return void
	 */
	public function updated(ProductSupplier $productSupplier)
	{
		//
	}

	/**
	 * Handle the product supplier "deleted" event.
	 *
	 * @param  \App\ProductSupplier  $productSupplier
	 * @return void
	 */
	public function deleted(ProductSupplier $productSupplier)
	{
		//
	}

	/**
	 * Handle the product supplier "deleting" event.
	 *
	 * @param  \App\ProductSupplier  $productSupplier
	 * @return void
	 */
	public function deleting(ProductSupplier $productSupplier)
	{
		//
		//throw new \Exception('prevent deleting');
	}

	/**
	 * Handle the product supplier "restored" event.
	 *
	 * @param  \App\ProductSupplier  $productSupplier
	 * @return void
	 */
	public function restored(ProductSupplier $productSupplier)
	{
		//
	}

	/**
	 * Handle the product supplier "force deleted" event.
	 *
	 * @param  \App\ProductSupplier  $productSupplier
	 * @return void
	 */
	public function forceDeleted(ProductSupplier $productSupplier)
	{
		//
	}
}
