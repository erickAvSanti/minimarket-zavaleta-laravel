<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleHistory extends Model
{
	//
	protected $fillable = [
		'sale_id',
		'sale_subtotal',
		'sale_total',
		'sale_created_at',
		'sale_updated_at',
		'json_products',
	];
	public function sale()
  {
   	return $this->belongsTo('App\Sale','sale_id');
  }

	public static function create_for(\App\Sale $sale){

		$tmp = $sale->toArray();
		$tmp['sale_id'] = $sale->id;
		$tmp['sale_created_at'] = $sale->created_at;
		$tmp['sale_updated_at'] = $sale->updated_at;
		$sale_history = new SaleHistory($tmp);
		
		$sale_history->save();
		return $sale_history;
	}

}
