<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSupplierShoppingRecord extends Model
{
	//
	use SoftDeletes;
	protected $fillable = [
		'product_id',
		'supplier_id',
		'total_price_paid',
		'bought_at',
		'is_purchased_individually',
		'price_by_item',
		'price_by_box',
		'how_many_items_bought_individually',
		'how_many_loose_products_were_bought',
		'how_many_boxes',
		'how_many_items_by_box',
		'receipt_type',
		'receipt_number',
		'barcode',
	];
	public function getIsPurchasedIndividuallyAttribute($value)
	{
	  return (bool) $value;
	}

	public static function &getFor($product_id, $supplier_id,$params){
		$show_deleted = $params['show_deleted'];

		$show_deleted = $show_deleted === 'true' || $show_deleted === true;

		$records = ProductSupplierShoppingRecord::where([
				['product_id' , $product_id],
				['supplier_id' , $supplier_id],
			])->orderBy('created_at','desc');
		if( $show_deleted ){
			//$records = $records->withTrashed();
			$records = $records->onlyTrashed();
		}
		$records = $records->get();
		return $records;
	}

}
