<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'prod_name' => 'required|string|max:100|min:2',
			'prod_barcode' => "required|string|unique:products,prod_barcode,".$this->id."|min:2",
			'prod_price' => 'required|numeric|min:0',
			'prod_stock' => 'required|numeric|min:0|max:10000',
		];
	}

	/**
	* Get the error messages for the defined validation rules.
	*
	* @return array
	*/

	public function messages()
	{
	  return [
      'prod_name.required' => 'El nombre del producto es requerido',
      'prod_barcode.required'  => 'El código de barra del producto es requerido',
      'prod_barcode.unique'  => 'El código de barra del producto ya existe',
      'prod_barcode.string'  => 'El código de barra del producto debe ser una cadena de caracteres',
      'prod_barcode.min'  => 'El código de barra del producto debe ser una cadena de caracteres mayor a 1 caracter',
      //'prod_barcode.numeric'  => 'El código de barra del producto debe ser un número',
      //'prod_barcode.max'  => 'El código de barra del producto excede el máximo',
      //'prod_barcode.min'  => 'El código de barra del producto debe ser un número mayor o igual a cero',
      'prod_price.required'  => 'El precio de venta al público del producto es requerido',
      'prod_price.numeric'  => 'El precio de venta al público debe ser un número decimal mayor a cero',
      'prod_stock.required'  => 'El stock del producto es requerido',
      'prod_stock.numeric'  => 'El stock debe ser un número entero mayor a cero',
      'prod_stock.min'  => 'El stock debe ser un número entero mayor a cero',
      'prod_stock.max'  => 'El stock debe ser un número enterno que no exceda el máximo',
	  ];
	}

	/**
	* Get custom attributes for validator errors.
	*
	* @return array
	*/
	public function attributes()
	{
	  return [
	  	'prod_name' => 'Nombre del producto',
	  	'prod_barcode' => 'Código de barras del producto',
	  	'prod_price' => 'Precio de venta al público del producto',
	  	'prod_stock' => 'Stock del producto',
	  ];
	}
	/**
	* Prepare the data for validation.
	*
	* @return void
	*/
	protected function prepareForValidation()
	{
		$arr = [];
		if(!isset($this->prod_desc)){
			$arr['prod_desc'] = '';
		}
		if(!isset($this->prod_stock)){
			$arr['prod_stock'] = 0.0;
		}
	  $this->merge($arr);
	}
}
