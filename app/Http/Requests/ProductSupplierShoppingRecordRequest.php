<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Rules\BooleanString;

class ProductSupplierShoppingRecordRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}
	public function rules()
	{
		return [
			'product_id' => "required|integer|min:1",
			'supplier_id' => "required|integer|min:1",
			'total_price_paid' => "required|numeric|min:0",
			'is_purchased_individually' => ['required', new BooleanString],
			'price_by_item' => "required|numeric|min:0",
			'price_by_box' => "required|numeric|min:0",
			'how_many_items_bought_individually' => "required|integer|min:0",
			'how_many_boxes' => "required|integer|min:0",
			'how_many_items_by_box' => "required|integer|min:0",
			'bought_at' => "required|date_format:Y-m-d",
		];
	}

	/**
	* Get the error messages for the defined validation rules.
	*
	* @return array
	*/

	public function messages()
	{
	  return [
      'product_id.required'  => 'El ID del producto es requerido',
      'product_id.integer'  => 'El ID del producto debe ser un número entero',
      'product_id.min'  => 'El ID del producto debe ser un número mayor o igual a 1',

      'supplier_id.required'  => 'El ID del proveedor es requerido',
      'supplier_id.integer'  => 'El ID del proveedor debe ser un número entero',
      'supplier_id.min'  => 'El ID del proveedor debe ser un número mayor o igual a 1',
      
      'total_price_paid.required' => 'El precio total pagado es requerido',
      'total_price_paid.numeric' => 'El precio total pagado debe ser un número decimal',
      'total_price_paid.min' => 'El precio total pagado debe ser un valor mayor o igual a cero',
      
      'price_by_item.required' => 'El precio por item/kilo es requerido',
      'price_by_item.numeric' => 'El precio por item/kilo debe ser un número decimal',
      'price_by_item.min' => 'El precio por item/kilo debe ser un valor mayor o igual a cero',
      
      'price_by_box.required' => 'El precio por caja/paquete/saco/costal es requerido',
      'price_by_box.numeric' => 'El precio por caja/paquete/saco/costal debe ser un número decimal',
      'price_by_box.min' => 'El precio por caja/paquete/saco/costal debe ser un valor mayor o igual a cero',
	  
      'is_purchased_individually.required' => 'El campo es comprado individualmente es requerido',
      'is_purchased_individually.booleanstring' => 'El campo es comprado individualmente debe ser un valor boolean',
      
      'bought_at.required' => 'La fecha de adquisición de la compra es requerida',
      'bought_at.date_format' => 'La fecha de adquisición de la compra tiene un formato inválido',
	  ];
	}

	/**
	* Get custom attributes for validator errors.
	*
	* @return array
	*/
	public function attributes()
	{
	  return [
	  	'product_id' => 'ID del producto',
	  	'supplier_id' => 'ID del proveedor',
	  	'total_price_paid' => 'Precio total pagado',
	  	'is_purchased_individually' => 'Comprado por items',
	  	'price_by_item' => 'Precio por item o kilo',
	  	'price_by_box' => 'Precio por caja o paquete o saco o costal',
	  	'how_many_items_bought_individually' => 'Cuántos items o kilos comprados individualmente',
	  	'how_many_boxes' => 'Cuántas cajas hay',
	  	'how_many_items_by_box' => 'Cuántos items o kilos hay en la caja o paquete o saco o costal',
	  	'bought_at' => 'Fecha de adquisición de la compra',
	  ];
	}
	/**
	* Prepare the data for validation.
	*
	* @return void
	*/
	protected function prepareForValidation()
	{
		$arr = [];
		if(!$this->price_by_box)$arr['price_by_box'] = 0;
		if(!$this->price_by_item)$arr['price_by_item'] = 0;
		if(!$this->receipt_number)$arr['receipt_number'] = '';
		if(!$this->barcode)$arr['barcode'] = '';
		foreach ($this->all() as $key => $value) {
			if(preg_match("/^\d+$/",$value)){
				$this[$key] = (int)$value;
			}else if(is_numeric($value)){
				$this[$key] = floatval($value);
			}else if($value == 'true'){
				$this[$key] = true;
			}else if($value == 'false'){
				$this[$key] = false;
			}
		}

	  if(
	  	$this->bought_at && 
	  	preg_match('/(\d{4}-\d{2}-\d{2}).*/',$this->bought_at,$matches) === 1 
	  ){
	  	$arr['bought_at'] = $matches[1];
	  }
	  $this->merge($arr);
	}
}
