<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductSupplierRequest extends FormRequest
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'sup_name' => "required|string|unique:product_suppliers,sup_name,".$this->id."|max:100|min:2",
			'document_number' => "required|unique:product_suppliers,document_number,".$this->id."|max:11|min:8",
			'document_type' => 'required|string|in:DNI,RUC'
		];
	}

	/**
	* Get the error messages for the defined validation rules.
	*
	* @return array
	*/

	public function messages()
	{
	  return [
      'sup_name.required'  => 'El campo proveedor de productos es requerido',
      'sup_name.unique'  => 'El proveedor de productos ya eiste',
      'sup_name.max'  => 'El campo proveedor debe tener a lo más 100 caracteres',
      'sup_name.min'  => 'El campo proveedor debe tener al menos 2 caracteres',
	  	
	  	'document_number.required' => 'El número de documento es requerido',
	  	'document_number.unique' => 'El número de documento ya existe',
	  	'document_number.max' => 'El número de documento debe tener como máximo 11 caracteres',
	  	'document_number.min' => 'El número de documento debe tener como mínimo 8 caracteres',
	  
	  	'document_type.required' => 'El tipo de documento es requerido',
	  	'document_type.string' => 'El tipo de documento debe ser una cadena de texto',
	  	'document_type.in' => 'El tipo de documento debe ser un DNI o RUC',
	  ];
	}

	/**
	* Get custom attributes for validator errors.
	*
	* @return array
	*/
	public function attributes()
	{
	  return [
	  	'sup_name' => 'Proveedor de producto',
	  	'document_number' => 'Número de documento',
	  	'document_type' => 'Tipo de documento',
	  ];
	}
	/**
	* Prepare the data for validation.
	*
	* @return void
	*/
	protected function prepareForValidation()
	{
		$arr = [];
		if(!$this->address1)$arr['address1'] = '-';
		if(!$this->address2)$arr['address2'] = '-';
		if(!isset($this->sup_desc)){
			$arr['sup_desc'] = '';
		}
	  	$this->merge($arr);
	}
}
