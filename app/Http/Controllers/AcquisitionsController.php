<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Acquisition;
use \App\AcquisitionMeasurementUnit;
use \App\ProductSupplier;
use \App\Product;
use Illuminate\Support\Facades\DB;

class AcquisitionsController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}

	const ROWS_X_PAGES = 20;
	public function index(){
		return view('acquisitions.index');
	}
	public function list(Request $request){
		$page = 
			is_numeric($request->page) ? $request->page : 1;
		$show_deleted = 
			is_numeric($request->show_deleted) ? $request->show_deleted : 0;

		$records = new Acquisition;
		if( $request->search_string ){
			$records = $records->where(function($query) use($request){
				if(!empty($request->search_string)){
					$query->where('prod_barcode',$request->search_string)
						->orWhere('prod_name','LIKE',"%".$request->search_string."%");
				}
			});
		}
		if( $request->show_who_have_barcode ){
			$records = $records->where('prod_barcode','<>','');
		}

		$total_records = $show_deleted == 1 ? $records->onlyTrashed()->count() : $records->count();
		$total_pages = floor($total_records / self::ROWS_X_PAGES) + 1;
		$page = $page < 1 ? 1 : ( 
			$total_pages < $page ? $total_pages : $page 
		);
		$records = $show_deleted == 1 ? $records->onlyTrashed()->offset( 
			self::ROWS_X_PAGES * ( $page - 1 ) 
		)->limit(self::ROWS_X_PAGES) : $records->offset( 
			self::ROWS_X_PAGES * ( $page - 1 ) 
		)->limit(self::ROWS_X_PAGES);

		$rows = [];
		foreach ($records->get() as &$record) {
			$row = $record->toArray();
			$row['supplier'] = $record->product_supplier;
			$row['measurement_unit'] = $record->measurement_unit;
			$row['product'] = $record->product;
			$rows[] = $row;
		}

		return response()->json([
			'records' => $rows,
			'totalRows' => $total_records,
			'totalPages' => $total_pages,
			'rowsPerPage' => self::ROWS_X_PAGES,
			'currentPage' => $page,
			'measurement_units' =>  AcquisitionMeasurementUnit::toOptions(),
		]);
	}
	public function find_supplier(Request $request){
		$query_string = $request->input('query');
		$records = new ProductSupplier;
		if(is_numeric($query_string)){
			$records = $records->where('document_number',
				$query_string);
		}else{
			$records = $records->where('sup_name','LIKE',
				"%$query_string%");
		}

		return response()->json([
			'records' => $records->get(),
		]);
	}
	public function find_prod_barcode(Request $request){
		$query_string = $request->input('query');
		return response()
			->json(
				Product::where("prod_barcode",$request->input('query'))
				->get()
			);
	}
	public function excel_report(Request $request)
	{
		return \Redirect::to(Acquisition::gen_excel_report_by($request));
	}

	public function update_or_create(Request $request){
		try{
			if( $request->id ){
				$record = Acquisition::find( $request->id );
			}else{
				$record = new Acquisition;
			}
			$record->fill($request->all());
			DB::transaction(function() use($record, $request){
				$record->save();
				if($request->sync_prod_name == 'yes' && $request->prod_barcode && strlen($request->prod_barcode)>5){
					$product_to_update = Product::where('prod_barcode',$request->prod_barcode)->first();
					if( $product_to_update ){
						$product_to_update->prod_name = $request->prod_name;
						if($product_to_update->prod_price != $request->prod_unit_price_for_sale){
							$product_to_update->prod_price = $request->prod_unit_price_for_sale;
						}
						if( $request->sync_prod_quantity == 'yes' ){
							$product_to_update->prod_stock += (int)$request->prod_quantity;
						}
						$product_to_update->save();
					}
				}else{
					if($request->prod_barcode && strlen($request->prod_barcode)>5){
						$product_exist = Product::where('prod_barcode',$request->prod_barcode)->first();
						if(!$product_exist){
							$product_to_create = new Product;
							$product_to_create->prod_barcode = $request->prod_barcode;
							$product_to_create->prod_name = $request->prod_name;
							$product_to_create->prod_stock = $request->prod_quantity;
							$product_to_create->prod_price = $request->prod_unit_price_for_sale;
							$product_to_create->prod_desc = '';
							$product_to_create->save();
						}
					}
				}
			});
			return response()->json($record);
		}catch(\Exception $e){
			\Log::info($e->getTraceAsString());
			return response()->json([
				'msg' => $e->getMessage(),
			],500);
		}
	}
	public function delete($id)
	{
		try{
			$record = Acquisition::withTrashed()->where('id',$id)->first();
			if($record->trashed()){
				$record->restore();
			}else{
				$record->delete();
			}
			return response()->json($record);
		}catch(\Exception $e){
			return response()->json([
				'msg' => $e->getMessage(),
			],500);
		}
	}
}
