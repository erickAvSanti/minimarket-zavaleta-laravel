<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BackUpDBController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
    }

    public function process(Request $request){
        if(!$request->user()->is_admin){
            abort(403);
        }
        $directory = \App\Helpers\StorageHelper::make_accessible_directory(storage_path('app/public/request_backups/'));
        \App\Helpers\StorageHelper::remove_files_from_directory_who_are_older_than($directory,0.2);//2nd param as day
        $file_path = \App\Helpers\MariaDBHelper::make_a_full_db_backup_to_directory($directory);
        if($file_path){
            //return Redirect::to(asset('storage/request_backups/'.$filename));
            return response()->download($file_path);
        }
    }
}