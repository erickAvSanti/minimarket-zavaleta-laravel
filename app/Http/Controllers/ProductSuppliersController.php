<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductSupplierRequest;
use App\Http\Requests\ProductSupplierShoppingRecordRequest;
use \App\ProductSupplier;
use \App\ProductSupplierShoppingRecord;
use \App\ProductSupplierAcquisition;

class ProductSuppliersController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(Request $request)
	{
		$order_by = $request->input('order_by') ?? '';
		$order = $request->input('order') ?? '';
		if(!empty($request->input('query'))){
			$records = ProductSupplier::where(function ($query) use ($request) {
     			$query->where('sup_name','LIKE',"%".$request->input('query')."%");
    		});
			//$records = ProductSupplier::where('sup_name','LIKE',"%".$request->input('query')."%");
			if(in_array($order,['asc','desc'])){
				$records = $records->orderBy($order_by,$order);
			}
		}else{
			if(empty($order_by) && empty($order)){
				$records = ProductSupplier::select('*');
			}else{
				if(!empty($order_by)){
					$records = ProductSupplier::orderBy($order_by,$order);
				}
			}
		}
		if(isset($request->show_deleted)){
			$records = $records->onlyTrashed();
		}
		return view('product_suppliers',[
			'records' => $records->get(), 
			'query' => ( $request->input('query') ?? '' ),
			'order_by' => $order_by,
			'order' => $order,
			'show_deleted' => $request->show_deleted,
		]);
	}
	public function add(Request $request)
	{
		$props = $request->all();
		return view('product_suppliers.form_add',['props' => $props]);
	}
	public function show(Request $request)
	{
		$record = ProductSupplier::find($request->id);
		return view('product_suppliers.form_edit',[
			'record' => $record,
		]);
	}
	public function create(ProductSupplierRequest $request)
	{
		$validated = $request->validated();
		//$props = $request->all();
		//return response()->json($request->all());
		$record = new ProductSupplier();
		$record->fill($request->all())->save();
		if ($request->isJson())
		{
			return response()->json($record->toArray());
		}else{
			return view('product_suppliers.form_success_created',[
				'record' => $record,
			]);
		}
	}
	public function update(ProductSupplierRequest $request, $id)
	{
		$validated = $request->validated();
		$record = ProductSupplier::find($id);
		$record->fill($request->all())->save();

		if ($request->isJson())
		{
			return response()->json($record->toArray());
		}else{
			return view('product_suppliers.form_success_updated',[
				'record' => $record,
			]);
		}
	}
	public function delete($id)
	{
		$record = ProductSupplier::withTrashed()->find($id);
		$deleted = false;
		$is_trashed = false;
		if($record){
			if($record->trashed()){
				$is_trashed = true;
				$deleted = $record->restore();
			}else{
				$deleted = $record->delete();
			}
		}

		return view('product_suppliers.delete_status',[
			'record' => $record,
			'deleted' => $deleted,
			'is_trashed' => $is_trashed,
		]);
	}

	public function purchase_update(
		ProductSupplierShoppingRecordRequest $request, 
		$id
	){
		( $record = ProductSupplierShoppingRecord::find($id) )->fill($request->all());
		return response()->json([
			'props' => $request->all(),
			'id' => $id,
			'record' => $record,
		]);
	}

	public function purchase_create(
		ProductSupplierShoppingRecordRequest $request
	){
		($record = new ProductSupplierShoppingRecord($request->all()))->save();
		return response()->json([
			'props' => $request->all(),
			'record' => $record,
		]);
	}
	public function list_purchases(Request $request, $product_id, $supplier_id){
		$records = &ProductSupplierShoppingRecord::getFor(
				$product_id, 
				$supplier_id,
				[
					'show_deleted' => $request->show_deleted,
				]
			);

		return response()->json([
			'records' => $records,
			'props' => $request->all(),
			'product_id' => $product_id,
			'supplier_id' => $supplier_id,
		]);
	}
	public function delete_purchase($id){
		ProductSupplierShoppingRecord::find($id)->delete();
		return response()->json([
			'msg' => 'ok',
		]);
	}





	public function acquisitions(Request $request, $product_supplier_id)
	{
		if($request->headers->get('Accept') == 'application/json'){
			$supplier = ProductSupplier::find($product_supplier_id);
			if($supplier){
				$records = $supplier->acquisitions;
				return response()->json([
					'records' => $records,
					'supplier' => $supplier,
				]);
			}else{
				return response()->json([],404);
			}
		}else{
			$record = ProductSupplier::find($product_supplier_id);
			return view('product_suppliers.acquisitions.show',[
				'record' => $record,
			]);
		}
	}
	public function add_acquisition(Request $request, $product_supplier_id)
	{
		$record = new ProductSupplierAcquisition;
		$record->supplier_id = $product_supplier_id;
		$record->save();
		return response()->json($record->toArray());
	}
}
