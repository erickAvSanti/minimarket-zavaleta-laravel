<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\SyncServerToken;
use \App\Helpers\SyncServerHelper;

class SyncServerController extends Controller
{
	//
	public static function push(Request $request){

		$sync_token = $request->header('X-SYNC-TOKEN') ?? '';
		$sync_token = trim($sync_token);

		\Log::info("sync_token = $sync_token");
		if(empty($sync_token)){
			return response()->json(['msg'=>'empty token'],400);
		}
		if(!SyncServerToken::where('sync_token',$sync_token)->exists()){
			return response()->json(['msg'=>'token expired'],403);
		}

		\Log::info($request->all());

		$file = $request->file('remote_file');
		
		if($file instanceof \Illuminate\Http\UploadedFile){

			$path = $file->storeAs('database_backups_uploaded_from_clients',$file->getClientOriginalName());

			$full_path = storage_path("app/$path");
			
			if( file_exists($full_path) ){
				SyncServerHelper::bind_uploaded_file_to_db($full_path);
				return response()->json(['msg'=>'success']);
			}else{
				return response()->json([
					'msg'=>'file unsaved'],500);
			}
		}else{
			return response()->json([
				'msg'=>'uploaded instance file failed!'],500);
		}
	}
}
