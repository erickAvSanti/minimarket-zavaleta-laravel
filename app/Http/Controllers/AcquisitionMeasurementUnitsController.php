<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\AcquisitionMeasurementUnit;

class AcquisitionMeasurementUnitsController extends Controller
{
	//
	public function list(Request $request){
		return response()->json(AcquisitionMeasurementUnit::orderBy('mu_name','asc')->get());
	}
	public function to_options(Request $request){
		return response()->json(AcquisitionMeasurementUnit::toOptions());
	}
	public function update_or_create(Request $request){
		try{
			if( $request->id ){
				$record = AcquisitionMeasurementUnit::find( $request->id );
			}else{
				$record = new AcquisitionMeasurementUnit;
			}
			$record->mu_name = strtoupper($request->mu_name);
			$record->save();
			return response()->json($record);
		}catch(\Exception $e){
			\Log::info($e->getMessage());
			return response()->json([
				'msg' => $e->getMessage(),
			],500);
		}
	}
	public function delete($id)
	{
		try{
			$record = AcquisitionMeasurementUnit::find($id);
			$record->delete();
			return response()->json($record);
		}catch(\Exception $e){
			return response()->json([
				'msg' => $e->getMessage(),
			],500);
		}
	}
}
