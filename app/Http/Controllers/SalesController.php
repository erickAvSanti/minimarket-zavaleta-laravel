<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Sale;
use \Carbon\Carbon;

class SalesController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(Request $request)
	{
		return view('sales',Sale::getAll($request));
	}
	public function show(Request $request, $id)
	{
		$record = Sale::find($id);
		return view('sales.form_edit',[
			'id' => $id,
			'record' => $record,
			'histories' => ($record ? $record->histories()->orderBy('created_at','desc')->get() : []),
		]);
	}
	public function printing_mode(Request $request)
	{
		$record = Sale::find($request->id);
		return view('sales.printing_mode',[
			'record' => $record,
		]);
	}
	public function add(Request $request)
	{
		$props = $request->all();
		return view('sales.form_add',['props' => $props]);
	}
	public function create(Request $request)
	{
		$record = Sale::create_for_products($request->products ?? []);
		if(!$record){
			return response()->json(['msg'=>'sale-not-created'],400);
		}
		return response()->json($record->toArray());
	}
	public function update(Request $request, $id)
	{
		$result = Sale::try_to_update($id, $request->products ?? [],$record);
		if($result == 'success'){
			return response()->json($record);
		}else{
			return response()->json(['msg' => $result], 400);
		}
	}
	public function excel_report(Request $request)
	{
		$res = Sale::gen_excel_report_by(
			$request->by, 
			$request
			);
		if($res){
			return \Redirect::to($res);
		}else{
			abort(404);
		}
	}
	public function delete($id)
	{
		$record = Sale::withTrashed()->find($id);
		$deleted = false;
		$is_trashed = false;
		if($record){
			if($record->trashed()){
				$is_trashed = true;
				$deleted = Sale::staticRestore($record);
			}else{
				$deleted = Sale::staticDelete($record);
			}
		}

		return view('sales.delete_status',[
			'record' => $record,
			'deleted' => $deleted,
			'is_trashed' => $is_trashed,
		]);
	}
}
