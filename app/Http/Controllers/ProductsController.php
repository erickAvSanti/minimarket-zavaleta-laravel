<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use \App\Product;
use \App\ProductSupplier;
use \App\ProductSupplierPrice;

use \App\Helpers\ProductHelper;

class ProductsController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	const ROWS_X_PAGES = 20;
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index(Request $request)
	{

		$page = 
			is_numeric($request->page) ? $request->page : 1;
		$rows_x_page = 
			$request->rows_x_page && 
			is_numeric($request->rows_x_page) ? 
			$request->rows_x_page : self::ROWS_X_PAGES;

		$order_by = $request->input('order_by') ?? '';
		$order = $request->input('order') ?? '';
		if(!empty($request->input('query'))){
			$records = Product::where(function ($query) use ($request) {
               $query->where('prod_name','LIKE',"%".$request->input('query')."%")
               ->orWhere('prod_barcode','LIKE',"%".$request->input('query')."%");
           });
			//$records = Product::where('prod_name','LIKE',"%".$request->input('query')."%")->orWhere('prod_barcode','LIKE',"%".$request->input('query')."%");
			if(in_array($order,['asc','desc'])){
				$records = $records->orderBy($order_by,$order);
			}
		}else{
			if(empty($order_by) && empty($order)){
				$records = Product::select('*');
			}else{
				if(!empty($order_by)){
					$records = Product::orderBy($order_by,$order);
				}
			}
		}
		if(isset($request->show_deleted)){
			$records = $records->onlyTrashed();
		}
		if($request->show_who_have_acquisitions === '1'){
			$records = $records->whereHas('acquisitions_by_barcode');
		}
		if($request->show_who_have_acquisitions === '2'){
			$records = $records->doesntHave('acquisitions_by_barcode');
		}
		$total_records = $records->count();
		$total_pages = floor($total_records / $rows_x_page) + 1;
		$page = $page < 1 ? 1 : ( 
			$total_pages < $page ? $total_pages : $page 
		);

		\Log::info($records->toSql());

		$records = $records->offset( 
			$rows_x_page * ( $page - 1 ) 
		)->limit( $rows_x_page );

		return view('products',[
			'records' => $records->get(), 
			'query' => ( $request->input('query') ?? '' ),
			'order_by' => $order_by,
			'order' => $order,
			'totalRows' => $total_records,
			'totalPages' => $total_pages,
			'rowsPerPage' => $rows_x_page,
			'page' => $page,
			'show_deleted' => $request->show_deleted,
			'show_who_have_acquisitions' => $request->show_who_have_acquisitions,
		]);
	}
	public function add(Request $request)
	{
		$props = $request->all();
		return view('products.form_add',[
			'props' => $props
		]);
	}
	public function show(Request $request, $id)
	{
		$record = Product::find($id);
		if($request->wantsJson()){
			return response()->json($record);
		}else{
			return view('products.form_edit',[
				'record' => $record,
				'id' => $id,
			]);
		}
	}
	public function create(ProductRequest $request)
	{
		$validated = $request->validated();
		//$props = $request->all();
		$record = new Product($request->all());
		$record->save();

		return view('products.form_success_created',[
			'record' => $record,
			'suppliers' => ProductSupplier::all(),
		]);
	}
	public function update(ProductRequest $request)
	{
		$validated = $request->validated();
		$record = Product::find($request->id);
		$record->fill($request->all())->save();

		return view('products.form_success_updated',[
			'record' => $record,
		]);
	}
	public function delete($id)
	{
		$record = Product::withTrashed()->find($id);
		$deleted = false;
		$is_trashed = false;
		if($record){
			if($record->trashed()){
				$is_trashed = true;
				$deleted = $record->restore();
			}else{
				$deleted = $record->delete();
			}
		}

		return view('products.delete_status',[
			'record' => $record,
			'deleted' => $deleted,
			'is_trashed' => $is_trashed,
		]);
	}
	public function supplier_prices($id)
	{
		$record = Product::find($id);
		$suppliers_with_prices = $record->get_suppliers_with_prices();
		return view('products.form_supplier_prices',[
			'record' => $record,
			'suppliers' => ProductSupplier::all(),
			'suppliers_with_prices' => $suppliers_with_prices,
		]);
	}
	public function supplier_prices_update(Request $request, $id)
	{
		$record = Product::find($id);
		if(!$record){
			return view('products.supplier_prices_update_status_failed',[
				'record' => $record,
			]);
		}else{
			$suppliers_id = array_map( function( $arr ){
				return $arr['id'];
			}, ProductSupplier::all(['id'])->toArray() );

			$suppliers_prices = [];
			foreach ($request->all() as $key => $value) {
				if( preg_match( "/^sup_id_(\d+)$/" , $key, $matches ) === 1 ){
					$supplier_id = (int)$matches[1];
					if( in_array($supplier_id, $suppliers_id) ){
						$suppliers_prices[] = [
							'product_id' => $id,
							'supplier_id' => $supplier_id,
							'sup_price' => ( is_numeric($value) ? (float)$value : 0.0 ),
						];
					}
				}
			}

			try {
				\DB::transaction(function () use($id, $suppliers_prices) {
					$collection = ProductSupplierPrice::where('product_id', $id);
					if($collection->count()==0){
						foreach ($suppliers_prices as $arr) {
							(new ProductSupplierPrice($arr))->save();
						}
					}else{
						foreach ($suppliers_prices as $arr) {

							$prod_sup_price_record = ProductSupplierPrice::where([
																					[ 'product_id', $arr['product_id'] ],
																					[ 'supplier_id', $arr['supplier_id'] ],
																				])->first();

							if( !$prod_sup_price_record ){
								(new ProductSupplierPrice($arr))->save();
							}else{
								$prod_sup_price_record->sup_price = $arr['sup_price'];
								$prod_sup_price_record->save();
							}
						}
					}
				});
			} catch (\Exception $e) {
				\Log::info($e);
				return back()->withErrors( [
					"exception" => $e->getMessage(),
					"prices" => "Problemas al asignar los precios de los proveedores"] );
			}
			$supplier_prices_records = [];
			foreach ($record->prices()->get() as $reg) {
				
				$supplier_prices_records[] = [
					'supplier' => $reg->product_supplier,
					'supplier_price' => (float)$reg->sup_price,
				];
			}
			return view('products.supplier_prices_update_status_success',[
				'record' => $record,
				'suppliers_prices' => $supplier_prices_records,
			]);

		}
	}

	public function filter_by(Request $request){

		if(isset($request->type) && $request->type == 'prod_barcode' && !empty($request->filter_field_value)){
			return response()->json([
				'records' => Product::where('prod_barcode','=',"$request->filter_field_value")->get()->toArray(),
			]);
		}

		if(isset($request->type) && $request->type == 'prod_name' && !empty($request->filter_field_value)){
			return response()->json([
				'records' => Product::where('prod_name','LIKE',"%$request->filter_field_value%")->get()->toArray(),
			]);
		}
		return response()->json([
			'records' => [],
		]);
	}

	public function import_from_csv(Request $request){
		if(!app()->environment('local')){
			return abort(404);
		}
		return view('products.import_from_csv_status',Product::import_from_csv($request));
	}

	public function show_prices_by_supplier($product_id, $supplier_id){
		return view('products.show_prices_by_supplier',[
			'product' => Product::find($product_id),
			'supplier' => ProductSupplier::find($supplier_id),
		]);
	}
	public function excel_export_all(){
		return response()->file(ProductHelper::excel_export_all());
	}
	public function excel_export_by_filter(Request $request){
		return response()->download(ProductHelper::excel_export_by_filter($request));
	}
}
