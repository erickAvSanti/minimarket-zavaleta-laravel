<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

use \App\Helpers\SaleHelper;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use \App\Product;
use Illuminate\Http\Request;

define('STORAGE_EXCEL_REPORT', storage_path() . '/app/public/excel_reports');
define('STORAGE_EXCEL_REPORT_DAILY', STORAGE_EXCEL_REPORT . '/daily');
if(!is_dir(STORAGE_EXCEL_REPORT_DAILY)){
	mkdir(STORAGE_EXCEL_REPORT_DAILY,0711,true);
}

class Sale extends Model
{
	//
	const ROWS_X_PAGES = 20;

	use SoftDeletes;
	protected $fillable = [
		'sale_subtotal',
		'sale_total',
		'json_products',
	];
	public function histories()
	{
		return $this->hasMany('App\SaleHistory','sale_id');
	}

	public static function getAll(Request $request){
		$page = $request->page && is_numeric($request->page) ? $request->page : 1;
		$rows_x_page = $request->rows_x_page && is_numeric($request->rows_x_page) ? $request->rows_x_page : self::ROWS_X_PAGES;
		$order_by = $request->input('order_by') ?? '';
		$order = $request->input('order') ?? '';
		$created_at_from = $request->created_at_from ?? null;
		$created_at_to = $request->created_at_to ?? null;
		if(!empty($request->search)){
			$records = Sale::where(function ($query) use ($request) {
               $query->where('json_products','LIKE',"%".$request->search."%");
           });
			//$records = Sale::where('field_name','LIKE',"%".$request->input('query')."%");
			if(in_array($order,['asc','desc'])){
				$records = $records->orderBy($order_by,$order);
			}
		}else{
			if(empty($order_by) && empty($order)){
				$records = Sale::orderBy('created_at','desc');
			}else{
				if(!empty($order_by)){
					$records = Sale::orderBy($order_by,$order);
				}
			}
		}

		if( 
			$created_at_from && 
			preg_match("/\d{2}\/\d{2}\/\d{4}/", $created_at_from) === 1 
		){
			$created_at_from 	= Carbon::createFromFormat("!d/m/Y",$created_at_from)
													->addHours(5)
													->toDateTimeString();

			$records = $records->where('created_at','>=',$created_at_from);
		}
		if( 
			$created_at_to && 
			preg_match("/\d{2}\/\d{2}\/\d{4}/", $created_at_to) === 1 
		){
			$created_at_to 	= Carbon::createFromFormat("!d/m/Y",$created_at_to)
												->addHours(5)
												->addDays(1)
												->toDateTimeString();

			$records = $records->where('created_at','<=',$created_at_to);
		}

		if(isset($request->show_deleted)){
			$records = $records->onlyTrashed();
		}
		if($request->id){
			$records = $records->where('id',$request->id);
		}
		if($request->sale_price_from){
			$records = $records->where('sale_total','>=',$request->sale_price_from);
		}
		if($request->sale_price_to){
			$records = $records->where('sale_total','<=',$request->sale_price_to);
		}

		$totalRows = $records->count();
		$totalPages = floor($totalRows / $rows_x_page) + 1;
		$page = $page < 1 ? 1 : ( 
			$totalPages < $page ? $totalPages : $page 
		);

		$rows = $records->offset( 
			$rows_x_page * ( $page - 1 ) 
		)->limit($rows_x_page);


		return [
			'records' => $rows->get(), 
			'totalRows' => $totalRows, 
			'totalPages' => $totalPages, 
			'rowsPerPage' => $rows_x_page, 
			'page' => $page, 
			'search' => $request->search,
			'order_by' => $order_by,
			'order' => $order,
			'show_deleted' => $request->show_deleted,
			'created_at_from' => $request->created_at_from,
			'created_at_to' => $request->created_at_to,
			'sale_price_from' => $request->sale_price_from,
			'sale_price_to' => $request->sale_price_to,
			'id' => $request->id,
		];
	}

	public function get_purchase_prices_of_products($omit_product_record = false){
		if($this->json_products){
			$products_arr = \json_decode($this->json_products,true);
			foreach($products_arr AS &$product_arr){
				$product_record = Product::where("prod_barcode",$product_arr['prod_barcode'])->first();
				if($product_record){
					$product_acquisitions = $product_record->acquisitions_by_barcode_to_calculate_purchase_price_order_by_bought_asc;
					$avg_purchase_price = 0;
					$product_acquisitions_taken_into_account = [];
					if(is_numeric(@$product_arr['prod_stock'])){
						$sum_quantity = 0;
						foreach($product_acquisitions AS &$product_acquisition){
							if( $product_acquisition->bought_at < $this->created_at ){
								$sum_quantity += $product_acquisition->prod_quantity;
								$avg_purchase_price += $product_acquisition->prod_unit_price;
								$product_acquisitions_taken_into_account[] = $product_acquisition;

								if($sum_quantity > $product_record->prod_stock / ( @$product_arr['prod_by_weight'] == 'yes' ? 1000.0 : 1) ){
									break;
								}
							}
						}
						if(count($product_acquisitions_taken_into_account)>0){
							$avg_purchase_price = $avg_purchase_price / count($product_acquisitions_taken_into_account);
						}
					}
					$product_arr['prod_purchase_price'] = $avg_purchase_price;
					if(!$omit_product_record)$product_arr['record'] = $product_record;
				}else{
					$product_arr['record'] = null;
					$product_arr['prod_purchase_price'] = 0;
				}
			}
			return $products_arr;
		}
		return null;
	}

	public static function create_for_products($arr){
		if(count($arr) == 0){
			return null;
		}
		try {
			$record = new Sale();
			DB::transaction(function() use($arr, $record){
				$sum = self::calc_total_values2($arr);
				$record->sale_total = $sum;
				$record->sale_subtotal = 0;

				$products = [];
				foreach ($arr as &$product_arr) {
					$product_id = $product_arr['id'];
					$product = Product::find($product_id);
					if($product){
						if(@$product_arr['prod_by_weight'] == 'yes'){

							$prod_quantity = (int)$product_arr['prod_quantity'];

							$val2 = 
								$product->prod_stock * 1000.0 + 
								(int)$product->prod_stock_no_unit - $prod_quantity;//4550

							$val3 = floor( $val2 / 1000.0 );//4
							$remaining_quantity = $val2 - $val3 * 1000.0;//550

							$product->prod_stock = $val3; // 4000
							$product->prod_stock_no_unit = $remaining_quantity;
							$product_arr['prod_stock'] = $product->prod_stock;
							$product_arr['prod_stock_no_unit'] = $product->prod_stock_no_unit;

						}else{
							
							$product->prod_stock -= (int)$product_arr['prod_quantity'];
							$product_arr['prod_stock'] = $product->prod_stock;
							
						}
						if($product->prod_stock < 0){
							throw new \Exception("Producto sin stock,producto: " . $product->prod_name . ", stock: " . $product->prod_stock);
							
						}
						$product_arr['prod_purchase_price'] = $product->avg_price_from_acquisitions_discounting_stock();
						$product->save();
					}

					$products[] = $product_arr;
				}
				$record->json_products = json_encode($products);

				$record->save();
			});
		} catch (\Exception $e) {
			\Log::info($e->getTraceAsString());
			$record = null;
		}
		return $record;
	}
	public static function try_to_update(
		$sale_id, 
		$new_products,
		&$record
	){
		$sale = Sale::find($sale_id);
		if(!$sale)return 'not-found';
		$record = $sale;
		$current_products = json_decode($sale->json_products, true);
		$current_products = $current_products ?? [];
		$with_history = false;
		$set_products = false;
		if(count($current_products) == 0){
			$set_products = true;
		}else{
			if( count($new_products) != count($current_products) ){
				//CREAR HISTORICO
				$with_history = true;
				$set_products = true;
			}else{
				if( 
					Sale::there_is_any_difference_on_products_with_same_size(
						$new_products,
						$current_products
					)
				){
					//CREAR HISTORICO
					$with_history = true;
					$set_products = true;
				}
			}
		}
		try {
			\DB::transaction(function() use(
				$sale, 
				$with_history, 
				$set_products, 
				$new_products, 
				$current_products
			){
				if($with_history){
					SaleHistory::create_for($sale);
				}

				//buscar los productos que fueron retirados en la nueva lista y los que ya existen para actualizar registros
				$products_stock_applied = [];
				foreach ($current_products as &$current_product) {
					$current_product_found_on_new_list = false;
					foreach ($new_products as &$new_product) {
						if( $new_product['id'] == $current_product['id'] ){
							$current_product_found_on_new_list = true;
							break;
						}
					}
					$product = Product::find($current_product['id']);
					if(!$product){
						throw new \Exception("sales_update, product with ID = " . $product->id . " not found");
						
					}
					if(!$current_product_found_on_new_list){

						$product->add_to_stock(
							$current_product['prod_quantity'],
							@$current_product['prod_by_weight'] == 'yes'
						);

					}else{
						$products_stock_applied[] = (int)$product->id;
						$product->add_to_stock(
							$current_product['prod_quantity'],
							@$current_product['prod_by_weight'] == 'yes'
						);
						
						$product->add_to_stock(
							-(int)$new_product['prod_quantity'],
							@$new_product['prod_by_weight'] == 'yes'
						);
						
						$new_product['prod_stock'] = $product->prod_stock;
						$new_product['prod_stock_no_unit'] = $product->prod_stock_no_unit;

					}
					$product->save();
				}
				foreach ($new_products as $kk => &$new_product) {
					$new_product_found_on_current_list = false;
					foreach ($current_products as &$current_product) {
						if( $new_product['id'] == $current_product['id'] ){
							$new_product_found_on_current_list = true;
							break;
						}
					}
					if( in_array((int)$new_product['id'], $products_stock_applied) ){
						continue;
					}
					$product = Product::find($new_product['id']);
					if(!$product){
						throw new \Exception("sales_update2, product with ID = " . $product->id . " not found");
						
					}
					if(!$new_product_found_on_current_list){

						$product->add_to_stock(
							-(int)$new_product['prod_quantity'],
							@$new_product['prod_by_weight'] == 'yes'
						);

					}
					$new_products[$kk]['prod_stock'] = $product->prod_stock;
					$new_products[$kk]['prod_stock_no_unit'] = $product->prod_stock_no_unit;

					$product->save();
				}

				if($set_products){
					$sale->calc_total_values($new_products);
					$sale->json_products = json_encode($new_products);
				}

				$sale->save();
			});
			return 'success';
		} catch (\Exception $e) {
			\Log::info($e->getTraceAsString());
			return 'error';
		}
	}
	public static function there_is_any_difference_on_products_with_same_size(
		$new_products, 
		$current_products
	){
		foreach ($current_products as $current_product) {
			$exist_id = false;
			$prop_changed = false;
			//ID
			foreach ($new_products as $new_product) {
				$prop_changed = false;
				if( (int)$current_product['id'] == (int)$new_product['id'] ){
					$exist_id = true;
					if( $current_product['prod_name'] != $new_product['prod_name'] ){
						$prop_changed = true;
					}
					if( $current_product['prod_barcode'] != $new_product['prod_barcode'] ){
						$prop_changed = true;
					}
					if( $current_product['prod_price'] != $new_product['prod_price'] ){
						$prop_changed = true;
					}
					if( @$current_product['prod_quantity'] != @$new_product['prod_quantity'] ){
						$prop_changed = true;
					}
					if( @$current_product['prod_weight_type'] != @$new_product['prod_weight_type'] ){
						$prop_changed = true;
					}
					if( @$current_product['prod_by_weight'] != @$new_product['prod_by_weight'] ){
						$prop_changed = true;
					}
					if( @$current_product['discount_sale'] != @$new_product['discount_sale'] ){
						$prop_changed = true;
					}
					if( @$current_product['discount_sale_price'] != @$new_product['discount_sale_price'] ){
						$prop_changed = true;
					}
					break;
				}
			}
			if(!$exist_id || $prop_changed)return true;
		}
		return false;
	}
	public static function calc_total_values2(&$products){
		$sum = 0;
		foreach ($products as $product) {
			/*
			if(@$product['prod_by_weight'] != 'yes'){
				$sum += (float)$product['prod_price'] * (float)$product['prod_quantity'];
			}else{
				$sum += (float)$product['prod_price'] * ( (float)$product['prod_quantity'] / 1000.0 );
			}
			*/
			if(!isset($product['discount_sale']) || @$product['discount_sale'] == 'no'){
				if(@$product['prod_by_weight'] != 'yes'){
					$sum += (float)$product['prod_price'] * (float)$product['prod_quantity'];
				}else{
					$sum += (float)$product['prod_price'] * ( (float)$product['prod_quantity'] / 1000.0 );
				}
			}else{
				$sum += (float)@$product['discount_sale_price'];
			}
		}
		return $sum;
	}
	public static function &get_all_from_today(){
		$date_now = Carbon::now()->addHours(-5)->format("d/m/Y");
		$records = &self::get_from_dates($date_now,$date_now);

		return $records;

	}
	public static function &get_from_dates(
		$from_date, 
		$to_date = null
	){
		if(!$to_date) $to_date = $from_date;
		
		$from_date 	= Carbon::createFromFormat("!d/m/Y",$from_date)
									->addHours(5)
									->toDateTimeString();

		$to_date 		= Carbon::createFromFormat("!d/m/Y",$to_date)
									->addHours(5)
									->addDays(1)
									->toDateTimeString();

		\Log::info($from_date);
		\Log::info($to_date);
		$records = (new Sale)
			->where('created_at','>=',$from_date)
			->where('created_at','<=',$to_date);
		$records = $records->get();
		return $records;

	}
	public static function &get_from_ids(
		$from_id, 
		$to_id
	){
		$records = (new Sale)
			->where('id','>=',$from_id)
			->where('id','<=',$to_id);
		$records = $records->get();
		return $records;

	}

	public static function gen_excel_report_by($by, $request){
		$result = SaleHelper::get_sales_report($by, $request);
		return $result;
	}

	public function calc_total_values(&$products){
		return $this->sale_total = Sale::calc_total_values2($products);
	}
	public function format_sale_code(){
		return sprintf("%014d", $this->id);
	}

	public static function staticRestore($record){
		//return $record->restore();

		try {
			
			DB::transaction(function()use($record){


				$products = json_decode($record->json_products,true);
				
				foreach ($products as &$product_arr) {
					
					$product_id = @$product_arr['id'];
					$product = Product::find($product_id);
					if(!$product){
						throw new \Exception("Product with ID = '$product_id' not found");
					}

					$product->add_to_stock(
						-(int)$product_arr['prod_quantity'],
						@$product_arr['prod_by_weight'] == 'yes'
					);
					
					$product_arr['prod_stock'] = $product->prod_stock;
					$product_arr['prod_stock_no_unit'] = $product->prod_stock_no_unit;

					if($product->prod_stock < 0){
						throw new \Exception("sale_restore, Producto sin stock,producto: " . $product->prod_name . ", stock: " . $product->prod_stock);
					}
					$product->save();

				}
				$record->json_products = json_encode($products);
				$record->save();
				$record->restore();
			});

			return true;

		} catch (\Exception $e) {
			\Log::info($e->getTraceAsString());
			return false;
		}
		
	}

	public static function staticDelete($record){
		//return $record->delete();

		try {
			
			DB::transaction(function()use($record){


				$products = json_decode($record->json_products,true);
				
				foreach ($products as &$product_arr) {
					
					$product_id = @$product_arr['id'];
					$product = Product::find($product_id);
					if(!$product){
						throw new \Exception("Product with ID = '$product_id' not found");
					}

					$product->add_to_stock(
						$product_arr['prod_quantity'],
						@$product_arr['prod_by_weight'] == 'yes'
					);

					$product_arr['prod_stock'] = $product->prod_stock;
					$product_arr['prod_stock_no_unit'] = $product->prod_stock_no_unit;

					if($product->prod_stock < 0){
						throw new \Exception("sale_delete, Producto sin stock,producto: " . $product->prod_name . ", stock: " . $product->prod_stock);
						
					}
					$product->save();

				}
				$record->json_products = json_encode($products);
				$record->save();
				$record->delete();
			});

			return true;

		} catch (\Exception $e) {
			\Log::info($e->getTraceAsString());
			return false;
		}
		
	}

}
