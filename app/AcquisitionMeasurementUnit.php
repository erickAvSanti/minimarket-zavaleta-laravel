<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AcquisitionMeasurementUnit extends Model
{
	//
	protected $fillable = [
		'mu_name',
	];
	public function acquisitions()
	{
		return $this->hasMany('App\Acquisition','measurement_unit_id');
	}

	public static function toOptions(){
		$rows = [];
		foreach (self::orderBy('mu_name','asc')->get() as &$record) {
			$rows[] = [
				'text' => $record->mu_name,
				'value' => $record->id,
			];
		}
		return $rows;
	}
	public static function create_from_name($str){

		if(!\is_string($str))return null;

		$str = \strtoupper(trim(\preg_replace('/\s+/',' ',$str)));
		$record = AcquisitionMeasurementUnit::where('mu_name',$str)->first();
		if(!$record){
			$record = new AcquisitionMeasurementUnit;
			$record->mu_name = $str;
			$record->save();
		}
		return $record;
	}
}
