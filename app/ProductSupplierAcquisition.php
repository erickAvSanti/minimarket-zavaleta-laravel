<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductSupplierAcquisition extends Model
{
	//
	protected $fillable = [
		'supplier_id',
		'bougth_at',
		'stocked_at',
		'json_products',
	];
	public function product_supplier()
  {
   	return $this->belongsTo('App\ProductSupplier','supplier_id');
  }
}
