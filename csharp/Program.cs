﻿using System;
using System.Diagnostics;
using System.ComponentModel;

using System.IO;

/*
build production command:
dotnet publish -c Release -o dist
*/

namespace SyncServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //ExecuteCommand(@"C:\git-projects\minimarket-zavaleta-laravel\schedule.bat");
            ExecuteArtisanCommand();
        }
        static void ExecuteCommand(string command)
        {
            int exitCode;
            ProcessStartInfo processInfo;
            Process process;

            processInfo = new ProcessStartInfo("cmd.exe", "/c " + command);
            processInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden; //Hides GUI
            processInfo.CreateNoWindow = true;
            processInfo.UseShellExecute = false;
            // *** Redirect the output ***
            processInfo.RedirectStandardError = true;
            processInfo.RedirectStandardOutput = true;

            process = Process.Start(processInfo);
            process.WaitForExit();

            // *** Read the streams ***
            // Warning: This approach can lead to deadlocks, see Edit #2
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();

            exitCode = process.ExitCode;

            Console.WriteLine("output>>" + (String.IsNullOrEmpty(output) ? "(none)" : output));
            Console.WriteLine("error>>" + (String.IsNullOrEmpty(error) ? "(none)" : error));
            Console.WriteLine("ExitCode: " + exitCode.ToString(), "ExecuteCommand");
            process.Close();
        }
        static void ExecuteArtisanCommand()
        {
            //string path = Directory.GetCurrentDirectory();
            //string path = System.Reflection.Assembly.GetExecutingAssembly().CodeBase;
            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;

            //once you have the path you get the directory with:
            var directory = System.IO.Path.GetDirectoryName(path);
            Process process = new Process();
            //process.StartInfo.WorkingDirectory = "cd %cd%";
            process.StartInfo.FileName = "cmd.exe";

            //processInfo = new ProcessStartInfo("cmd.exe", @"cd %~dp0 && php C:\git-projects\minimarket-zavaleta-laravel\artisan sync:serve 1>> NUL 2>&1");
            process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden; //Hides GUI
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.UseShellExecute = false;
            // *** Redirect the output ***
            process.StartInfo.RedirectStandardInput = true;

            process.Start();
            process.StandardInput.WriteLine(@"cd "+directory);
            process.StandardInput.WriteLine(@"cd ../../");
            process.StandardInput.WriteLine("php.exe artisan schedule:run 1>> NUL 2>&1 &");
            //process.WaitForExit();
            process.Close();
        }

    }
}
