<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		$faker = Faker\Factory::create();

		$regs = [
			'Eco',
			'Mantequilla Gloria',
			'Fideos Lavagi',
			'Nescafé',
			'Atún Gloria',
			'Galleta OREO',
			'Galleta Margarita',
			'Ayudín chico',
			'Ayudín mediano',
		];

		for ($i=0; $i < count($regs)  ; $i++) { 
			DB::table('products')->insert([
			  'prod_name' => $regs[$i],
			  'prod_barcode' => $faker->isbn10,
			  'prod_desc' => ($faker->boolean ? $faker->text : ''),
			  'prod_price' => $faker->randomFloat(2,1,30),
			  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
	    	'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			]);
		}
	}
}
