<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ProductSupplierSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		//
		$faker = Faker\Factory::create();

		$regs = [
			'Makro',
			'Metro',
			'Unicachi',
		];

		for ($i=0; $i < count($regs)  ; $i++) { 
			DB::table('product_suppliers')->insert([
			  'sup_name' => $regs[$i],
			  'sup_desc' => ($faker->boolean ? $faker->text : ''),
			  'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
	    	'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			]);
		}
	}
}
