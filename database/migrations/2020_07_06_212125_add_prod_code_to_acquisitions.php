<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProdCodeToAcquisitions extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('acquisitions', function (Blueprint $table) {
			//
			$table->string('prod_barcode',30)
						->index('prod_barcode_idx');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('acquisitions', function (Blueprint $table) {
			//
			$table->dropColumn('prod_barcode');
		});
	}
}
