<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTimestampsSalesToHistories extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('sale_histories', function (Blueprint $table) {
			//
			$table->timestampTz('sale_created_at')->nullable();
			$table->timestampTz('sale_updated_at')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('sale_histories', function (Blueprint $table) {
			//
			$table->dropColumn('sale_created_at');
			$table->dropColumn('sale_updated_at');
		});
	}
}
