<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAcquisitionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('acquisitions', function (Blueprint $table) {
			$table->id();
			$table->timestampTz('bought_at')->nullable();
			$table->foreignId('supplier_id');
			$table->string('prod_name',120);
			$table->string('prod_group',120);
			$table->unsignedInteger('prod_quantity');
			$table->decimal('prod_unit_price',9,2);
			$table->unsignedInteger('igv_type');
			$table->decimal('total_price',9,2);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('acquisitions');
	}
}
