<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldHowManyLooseProductsWereBoughtToProductSupplierShoppingRecords extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_supplier_shopping_records', function (Blueprint $table) {
			//
			$table->unsignedInteger('how_many_loose_products_were_bought')->default(0);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_supplier_shopping_records', function (Blueprint $table) {
			//
			$table->dropColumn('how_many_loose_products_were_bought');
		});
	}
}
