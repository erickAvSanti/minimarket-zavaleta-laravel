<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSupplierShoppingRecordsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_supplier_shopping_records', function (Blueprint $table) {
			$table->id();
			$table->foreignId('product_id');
			$table->foreignId('supplier_id');
			$table->decimal('total_price_paid',9,2)->default(0);
			$table->timestampTz('bought_at')->nullable();
			$table->boolean('is_purchased_individually')->default(false);
			$table->decimal('price_by_item',9,2)->default(0);
			$table->decimal('price_by_box',9,2)->default(0);
			$table->unsignedBigInteger('how_many_items_bought_individually')->default(0);
			$table->unsignedBigInteger('how_many_boxes')->default(0);
			$table->unsignedBigInteger('how_many_items_by_box')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('product_supplier_shopping_records');
	}
}
