<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDocumentToAcquisitions extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('acquisitions', function (Blueprint $table) {
			//
			$table->string('document_number',50)->nullable()->index();
			$table->string('document_type',20)->nullable()->index();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('acquisitions', function (Blueprint $table) {
			//
			$table->dropColumn('document_number');
			$table->dropColumn('document_type');
		});
	}
}
