<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToProductSupplierShoppingRecords extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_supplier_shopping_records', function (Blueprint $table) {
			$table->string('receipt_number',40)->default('');
			$table->string('receipt_type',20)->default('');
			$table->string('barcode',20)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_supplier_shopping_records', function (Blueprint $table) {
			//
			$table->dropColumn('receipt_number');
			$table->dropColumn('receipt_type');
			$table->dropColumn('barcode');
		});
	}
}
