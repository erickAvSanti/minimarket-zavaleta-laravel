<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExtraFieldsToProductSuppliers extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('product_suppliers', function (Blueprint $table) {
			//
			$table->string('document_type',30)->nullable();
			$table->string('document_number',30)->unique()->nullable();
			$table->text('address1')->nullable();
			$table->text('address2')->nullable();
			$table->string('phone_number1',30)->default('');
			$table->string('phone_number2',30)->default('');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('product_suppliers', function (Blueprint $table) {
			//
			$table->dropColumn('document_type');
			$table->dropColumn('document_number');
			$table->dropColumn('address1');
			$table->dropColumn('address2');
			$table->dropColumn('phone_number1');
			$table->dropColumn('phone_number2');
		});
	}
}
