<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSaleHistoriesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sale_histories', function (Blueprint $table) {
			$table->id();
			$table->foreignId('sale_id');
			$table->decimal('sale_total',9,2);
			$table->decimal('sale_subtotal',9,2);
			$table->text('json_products');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('sale_histories');
	}
}
