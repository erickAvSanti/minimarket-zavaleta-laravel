<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddProdUnitPriceForSaleToAcquisitions extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('acquisitions', function (Blueprint $table) {
			//
			$table->decimal('prod_unit_price_for_sale',9,2);
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('acquisitions', function (Blueprint $table) {
			//
			$table->dropColumn('prod_unit_price_for_sale');
		});
	}
}
