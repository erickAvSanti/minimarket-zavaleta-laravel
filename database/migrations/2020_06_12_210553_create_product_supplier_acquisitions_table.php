<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSupplierAcquisitionsTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_supplier_acquisitions', function (Blueprint $table) {
			$table->id();
			$table->foreignId('supplier_id');
			$table->text('json_products')->nullable();
			$table->timestampTz('stocked_at')->nullable();
			$table->timestampTz('bought_at')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('product_supplier_acquisitions');
	}
}
