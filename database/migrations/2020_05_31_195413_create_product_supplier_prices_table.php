<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductSupplierPricesTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('product_supplier_prices', function (Blueprint $table) {
			$table->id();
			$table->foreignId('product_id');
			$table->foreignId('supplier_id');
			$table->decimal('sup_price',9,2);
			$table->unique(['product_id','supplier_id'], 'unique_product_supplier_price');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('product_supplier_prices');
	}
}
