<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


$route_options = [];
if(app()->environment() !== 'local'){
  $route_options['register'] = false;
  $route_options['reset'] = false;
  $route_options['confirm'] = false;
  $route_options['verify'] = false;
}
Auth::routes($route_options);

Route::get('/home', 'HomeController@index')->name('home');

Route::prefix('productos')->group(function () {
  Route::get('/', 'ProductsController@index')->name('products');
  Route::get('/nuevo', 'ProductsController@add')->name('products_new');
  Route::get('/{product_id}/precios-de-proveedores/{supplier_id}', 'ProductsController@show_prices_by_supplier')->name('product_show_prices_by_supplier')->where(['product_id' => '\d+', 'supplier_id' => '\d+']);
  
  //DEPRECATED
  Route::get('/precios-de-proveedores/{id}', 'ProductsController@supplier_prices')->name('product_suppliers_prices_view')->where('id', '\d+');
  Route::post('/precios-de-proveedores/{id}', 'ProductsController@supplier_prices_update')->name('product_suppliers_prices_update')->where('id', '\d+');
  //END DEPRECATED
  Route::get('/{id}', 'ProductsController@show')->name('products_show')->where('id', '\d+');
  Route::get('/filtrar_por', 'ProductsController@filter_by')->name('products_filter_by');
  Route::post('/eliminar/{id}', 'ProductsController@delete')->name('products_delete')->where('id', '\d+');
  Route::post('/crear', 'ProductsController@create')->name('products_create');
  Route::post('/{id}', 'ProductsController@update')->name('products_update')->where('id', '\d+');
  Route::post('/importar-desde-csv', 'ProductsController@import_from_csv')->name('products_import_from_csv');
  
  Route::prefix('exportar-excel')->group(function(){
    Route::get('/todo', 'ProductsController@excel_export_all')->name('products_excel_export_all');
    Route::get('/by', 'ProductsController@excel_export_by_filter')->name('products_excel_export_by_filter');
  });

});

Route::group(['middleware' => ['cors']], function () {
    //Rutas a las que se permitirá acceso
});

Route::prefix('proveedores-productos')->group(function () {
  Route::get('/', 'ProductSuppliersController@index')->name('product_suppliers');
  Route::get('/nuevo', 'ProductSuppliersController@add')->name('product_suppliers_new');
  Route::get('/{id}', 'ProductSuppliersController@show')->name('product_suppliers_show')->where('id', '\d+');
  Route::post('/eliminar/{id}', 'ProductSuppliersController@delete')->name('product_suppliers_delete')->where('id', '\d+');
  Route::post('/crear', 'ProductSuppliersController@create')->name('product_suppliers_create');
  Route::post('/{id}', 'ProductSuppliersController@update')->name('product_suppliers_update')->where('id', '\d+');


  //BY ONE TYPE OF PRODUCT
  Route::prefix('compras')->group(function () {
    Route::get('/', 'ProductSuppliersController@purchases')->name('product_suppliers_purchases');
    Route::post('/{id}', 'ProductSuppliersController@purchase_update')->name('product_suppliers_purchase_update')->where('id', '\d+');
    Route::post('/', 'ProductSuppliersController@purchase_create')->name('product_suppliers_purchase_create');
    Route::get('/{product_id}/{supplier_id}', 'ProductSuppliersController@list_purchases')->name('product_suppliers_list_purchases')->where(['product_id' => '\d+', 'supplier_id' => '\d+']);
    Route::delete('/{id}', 'ProductSuppliersController@delete_purchase')->name('product_suppliers_purchase_delete')->where('id', '\d+');
  });

  //BY MANY PRODUCTS
  Route::prefix('adquisiciones')->group(function () {
    
    Route::get('/', 'AcquisitionsController@index')->name('acquisitions');
    Route::get('/exportar-excel-todo', 'AcquisitionsController@excel_report');
    Route::get('/list', 'AcquisitionsController@list');
    Route::get('/find_supplier', 'AcquisitionsController@find_supplier');
    Route::get('/find_prod_barcode', 'AcquisitionsController@find_prod_barcode');
    Route::post('/', 'AcquisitionsController@update_or_create');
    Route::delete('/{id}', 'AcquisitionsController@delete')->where(['id' => '^\d+$']);

    Route::get('/{product_supplier_id}', 'ProductSuppliersController@acquisitions')->name('product_suppliers_acquisitions')->where('product_supplier_id', '\d+');
    Route::post('/{product_supplier_id}', 'ProductSuppliersController@add_acquisition')->name('product_suppliers_add_acquisition')->where('product_supplier_id','\d+');
  
    Route::prefix('unidades-de-medida')->group(function(){

      Route::get('/', 'AcquisitionMeasurementUnitsController@index')->name('measurement_units');
      Route::get('/list', 'AcquisitionMeasurementUnitsController@list');
      Route::get('/to_options', 'AcquisitionMeasurementUnitsController@to_options');
      Route::post('/', 'AcquisitionMeasurementUnitsController@update_or_create');
      Route::delete('/{id}', 'AcquisitionMeasurementUnitsController@delete')->where(['id' => '^\d+$']);
      
    });

  });


});

Route::prefix('ventas')->group(function () {
  Route::get('/', 'SalesController@index')->name('sales');
  Route::get('/nuevo', 'SalesController@add')->name('sales_new');
  Route::get('/{id}', 'SalesController@show')->name('sales_show')->where('id', '\d+');
  Route::get('/imprimir/{id}', 'SalesController@printing_mode')->name('sales_printing_mode')->where('id', '\d+');
  Route::post('/crear', 'SalesController@create')->name('sales_create');
  Route::put('/{id}', 'SalesController@update')->name('sales_update')->where('id', '\d+');
  Route::get('/reportes-excel', 'SalesController@excel_report')->name('sales_excel_report');
  Route::post('/eliminar/{id}', 'SalesController@delete')->name('sales_delete')->where('id', '\d+');

});

Route::prefix('usuarios')->group(function () {
  Route::get('/', 'UsersController@index')->name('users');
  Route::get('/nuevo', 'UsersController@add')->name('users_new');
  Route::get('/{id}', 'UsersController@show')->name('users_show')->where('id', '\d+');
  Route::post('/eliminar/{id}', 'UsersController@delete')->name('users_delete')->where('id', '\d+');
  Route::post('/crear', 'UsersController@create')->name('users_create');
  Route::post('/{id}', 'UsersController@update')->name('users_update')->where('id', '\d+');
});

Route::get('backup_db','BackUpDBController@process');