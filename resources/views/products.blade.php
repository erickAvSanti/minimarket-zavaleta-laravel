@extends('layouts.app')

@section('content')
@include('products.modal_excel_export')
<div class="card">
	<div class="card-header">
		<label>Productos</label>
		<a href="{{ route('products_new') }}">
			<button class="btn btn-primary btn-sm">Agregar Producto</button>
		</a>
		<button 
			type="button" 
			class="btn btn-success btn-sm" 
			data-toggle="modal" 
			data-target="#modal_excel_export">
		  <i class="fa fa-file-excel-o"></i>
		</button>
		@if(app()->environment('local'))
		<div>
			<form 
				enctype="multipart/form-data" 
				method="post" 
				action="{{ route('products_import_from_csv') }}">
				@csrf
				<input type="file" name="csv_file">
				<button type="submit" class="btn btn-sm btn-primary">Subir</button>
			</form>
		</div>
		@endif
	</div>

	<div class="card-body">
		<form id="products_form" method="GET" action="">
			<input type="hidden" name="order_by" value="{{ $order_by ?? '' }}">
			<input type="hidden" name="order" value="{{ $order ?? '' }}">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text" id="basic-prod-search">
			    	<i class="fa fa-search"></i>
			    </span>
			  </div>
			  <input 
			  	name="query"
			  	type="text" 
			  	class="form-control" 
			  	name="query"
			  	value="{{ $query ?? ''}}"
			  	placeholder="Búsqueda" 
			  	aria-label="Búsqueda de productos" 
			  	aria-describedby="basic-prod-search" />
				  <div class="input-group-append">
				    <button class="btn btn-success" id="basic-prod-search">
				    	Buscar
				    </button>
				  </div>
			</div>
			<div class="form-check noselect">
			  <input 
			  	class="form-check-input" 
			  	type="checkbox" 
			  	value="1" 
			  	name="show_deleted" 
			  	{{ isset($show_deleted) ? 'checked' : '' }} 
			  	id="show_deleted" />
			  <label class="form-check-label" for="show_deleted">
			    Buscar en eliminados
			  </label>
			</div>
			<!--
			<div class="form-check noselect">
			  <input 
			  	class="form-check-input" 
			  	type="checkbox" 
			  	value="1" 
			  	name="show_who_have_acquisitions" 
			  	{{ isset($show_who_have_acquisitions) ? 'checked' : '' }} 
			  	id="show_who_have_acquisitions" />
			  <label class="form-check-label" for="show_who_have_acquisitions">
			    Mostrar los que tienen adquisiciones
			  </label>
			</div>
			-->

			<div>
				<div class="custom-control custom-radio">
					<input 
						type="radio" 
						id="show_who_have_acquisitions0" 
						name="show_who_have_acquisitions" 
						value="0" 
						{{ $show_who_have_acquisitions == 0 ? 'checked' : '' }} 
						class="custom-control-input">
					<label 
						class="custom-control-label" 
						for="show_who_have_acquisitions0">
						Mostrar con/sin adquisiciones
					</label>
				</div>
				<div class="custom-control custom-radio">
					<input 
						type="radio" 
						id="show_who_have_acquisitions1" 
						name="show_who_have_acquisitions" 
						value="1" 
						{{ $show_who_have_acquisitions == 1 ? 'checked' : '' }} 
						class="custom-control-input">
					<label 
						class="custom-control-label" 
						for="show_who_have_acquisitions1">
						Mostrar los que tienen adquisiciones
					</label>
				</div>
				<div class="custom-control custom-radio">
					<input 
						type="radio" 
						id="show_who_have_acquisitions2" 
						name="show_who_have_acquisitions" 
						value="2" 
						{{ $show_who_have_acquisitions == 2 ? 'checked' : '' }} 
						class="custom-control-input">
					<label 
						class="custom-control-label" 
						for="show_who_have_acquisitions2">
						Mostrar los que NO tienen adquisiciones
					</label>
				</div>
			</div>
			<br />
			<div class="btn-group mb-1">
				<button 
					type="button" 
					class="btn btn-primary dropdown-toggle btn-sm" 
					data-toggle="dropdown" 
					aria-haspopup="true" 
					aria-expanded="false">
					Paginación de {{ $rowsPerPage }}
				</button>
				<div class="dropdown-menu products_rows_x_page">
					@foreach([20,50,100,150,200] as $rows_x_page)
					<a class="dropdown-item" rows_x_page="{{ $rows_x_page }}">{{ $rows_x_page }} por página</a>
					@endforeach
				</div>
			</div>
			<input type="hidden" name="page" value="1">
			<input type="hidden" name="rows_x_page" value="20">
			<nav aria-label="Paginación de búsqueda" class="nav_pagination">
				<ul class="pagination products_pagination">
					<li class="page-item {{ $page == 1 ? 'disabled' : '' }}" page="{{ $page - 1 }}">
						<a 
							class="page-link" 
							tabindex="-1" 
							href="{{ $page == 1 ? '' : '#' }}"
							aria-disabled="{{ $page == 1 ? 'true' : 'false' }}">&laquo;</a>
					</li>
					@for($i = 1; $i <= $totalPages; $i++)
						@if($i != $page)
						<li class="page-item" page="{{ $i }}">
							<a class="page-link">{{$i}}</a>
						</li>
						@else
						<li 
							class="page-item active"
							aria-current="page">
							<a class="page-link">
								{{$i}}
								<span class="sr-only">(current)</span>
							</a>
						</li>
						@endif
					@endfor
					<li page="{{ $page + 1 }}"
						class="
							page-item 
							{{ $page == $totalPages ? 'disabled' : '' }} 
						" 
					>
						<a 
							class="page-link" 
							aria-disabled="{{ $page == $totalPages ? 'true' : 'false' }}" 
							href="{{ $page == $totalPages ? '' : '#' }}"
						>&raquo;</a>
					</li>
				</ul>
			</nav><label>Total: {{ $totalRows }}</label>
		</form>
		<div class="table-container2">
			<table 
				class="
					table 
					table-hover 
					table-borderer 
					table-stripped">
				<thead class="noselect">
					<tr>
						<th>
							<button 
								class="btn btn-sm btn-success" 
								refresh-action 
								refresh-for="#products_form">
								<i class="fa fa-refresh"></i>
							</button>
						</th>
						<th 
							form_tag_id="#products_form" 
							order_by="prod_name" 
							order="{{ $order_by == 'prod_name' ? $order : '' }}">Producto</th>
						<th 
							form_tag_id="#products_form" 
							order_by="prod_barcode" 
							order="{{ $order_by == 'prod_barcode' ? $order : '' }}">Código de Barra</th>
						<th 
							form_tag_id="#products_form" 
							order_by="prod_price" 
							order="{{ $order_by == 'prod_price' ? $order : '' }}">Precio de venta</th>
						<th 
							form_tag_id="#products_form" 
							order_by="prod_stock" 
							order="{{ $order_by == 'prod_stock' ? $order : '' }}">Stock</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($records as $idx => $record)
						<tr>
							<td>{{ $rowsPerPage * ($page - 1) + $idx + 1 }}</td>
							<td>
								{{ $record->prod_name }}
								@if($show_who_have_acquisitions)
								<{{ $record->total_acquisitions_by_barcode() }}>
								<ul>
									@foreach( $record->acquisitions_by_barcode AS $acquisition )
									<li>{{ $acquisition->document_number }} <br />({{ $acquisition->product_supplier->sup_name }})</li>
									@endforeach
								</ul>
								@endif
							</td>
							<td>{{ $record->prod_barcode }}</td>
							<td>
								{{ $record->prod_price }}
								<ul>
									@foreach ($record->get_suppliers_with_prices() as $supplier_prices)
										@if( (float)$supplier_prices['price'] != 0.0 )
										<li>
											{{ $supplier_prices['supplier']->sup_name }} => 
											{{ $supplier_prices['price'] }}
										</li>
										@endif
									@endforeach
								</ul>
							</td>
							<td>
								{{ $record->prod_stock }}
								{{ $record->prod_stock_no_unit > 0 ? " + " . ( $record->prod_stock_no_unit / 1000.0 ) : '' }}</td>
							<td>
								<a href="{{ route('products_show',['id' => $record->id]) }}">
									<button class="btn btn-primary btn-sm">
										Ver
									</button>
								</a>
								<button 
									class="btn {{ isset($show_deleted) ? 'btn-warning' : 'btn-danger' }} btn-sm" 
									mod-name="product" 
									del-action 
									record-id="{{ $record->id }}"  
									record-name="{{ $record->prod_name }}">
									{{ isset($show_deleted) ? 'Deshacer eliminación' : 'Eliminar' }}
								</button>
								<form id="form_del_{{ $record->id }}" method="POST" action="{{ route('products_delete',['id' => $record->id]) }}">
									@csrf
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
