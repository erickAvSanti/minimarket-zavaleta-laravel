@extends('layouts.app')

@section('content')
<!-- START MODAL  -->
@include('sales.modal_excel_report')
<!-- END MODAL  -->
<div class="card">
	<div class="card-header">
		<label>Ventas</label>
		<a href="{{ route('sales_new') }}">
			<button class="btn btn-primary btn-sm">
				<i class="fa fa-plus"></i>
			</button>
		</a>
		<button 
			class="btn btn-success btn-sm" 
			data-toggle="modal" 
			data-target="#sales_report_excel_modal">
			Reportes <i class="fa fa-file-excel-o"></i>
		</button>
	</div>

	<div class="card-body">
		<form id="sales_form" method="GET" action="">
			<input type="hidden" name="order_by" value="{{ $order_by ?? '' }}">
			<input type="hidden" name="order" value="{{ $order ?? '' }}">
			<div class="form-check noselect">
				<input 
					class="form-check-input" 
					type="checkbox" 
					value="1" 
					name="show_deleted" 
					{{ isset($show_deleted) ? 'checked' : '' }} 
					id="show_deleted" />
				<label class="form-check-label" for="show_deleted">
					Mostrar eliminados
				</label>
			</div>
			<div class="row">
				<div class="col-lg-4">
					<div class="find_by_created_at custom_card">
						<h6>Búsqueda por fechas de creación</h6>
						<div>
							<div>
								<label>
									Desde 
									<div class="btn btn-sm btn-danger">
										<i class="fa fa-times"></i>
									</div>
								</label>
								<div class='input-group date' id='created_at_from'>
									<input 
										type='text' 
										class="form-control" 
										name="created_at_from" 
										value="{{ $created_at_from ?? '' }}" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</div>
							</div>
							<div>
								<label>
									Hasta 
									<div class="btn btn-sm btn-danger">
										<i class="fa fa-times"></i>
									</div>
								</label>
								<div class='input-group date' id='created_at_to'>
									<input 
										type='text' 
										class="form-control" 
										name="created_at_to" 
										value="{{ $created_at_to ?? '' }}" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-time"></span>
									</span>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="custom_card">
						<h6>Búsqueda por código o ticket de venta </h6>
						<div class="input-group mb-3">
							<input 
								type="number" 
								class="form-control" 
								name="id"
								placeholder="Código/Ticket" value="{{ $id }}" />
						</div>
					</div>
				</div>
				<div class="col-lg-4">
					<div class="find_by_sale_prices custom_card2">
						<h6>Búsqueda por rango de precios</h6>
						<div>
							<div>
								<label>
									Desde 
									<div class="btn btn-sm btn-danger">
										<i class="fa fa-times"></i>
									</div>
								</label>
								<input 
									type="number" 
									class="form-control" 
									name="sale_price_from"
									placeholder="Precio desde" 
									value="{{ $sale_price_from }}" />
							</div>
							<div>
								<label>
									Hasta 
									<div class="btn btn-sm btn-danger">
										<i class="fa fa-times"></i>
									</div>
								</label>
								<input 
									type="number" 
									class="form-control" 
									name="sale_price_to"
									placeholder="Precio hasta" 
									value="{{ $sale_price_to }}" />
							</div>
						</div>
					</div>
				</div>
			</div>
			<br />
			<div class="btn-group mb-1">
				<button 
					type="button" 
					class="btn btn-primary dropdown-toggle btn-sm" 
					data-toggle="dropdown" 
					aria-haspopup="true" 
					aria-expanded="false">
					Paginación de {{ $rowsPerPage }}
				</button>
				<div class="dropdown-menu sales_rows_x_page">
					@foreach([20,50,100,150,200] as $rows_x_page)
					<a class="dropdown-item" rows_x_page="{{ $rows_x_page }}">{{ $rows_x_page }} por página</a>
					@endforeach
				</div>
			</div>
			<input type="hidden" name="page" value="1">
			<input type="hidden" name="rows_x_page" value="20">
			<nav aria-label="Paginación de búsqueda" class="nav_pagination">
				<ul class="pagination sales_pagination">
					<li class="page-item {{ $page == 1 ? 'disabled' : '' }}" page="{{ $page - 1 }}">
						<a 
							class="page-link" 
							tabindex="-1" 
							href="{{ $page == 1 ? '' : '#' }}"
							aria-disabled="{{ $page == 1 ? 'true' : 'false' }}">&laquo;</a>
					</li>
					@for($i = 1; $i <= $totalPages; $i++)
						@if($i != $page)
						<li class="page-item" page="{{ $i }}">
							<a class="page-link">{{$i}}</a>
						</li>
						@else
						<li 
							class="page-item active"
							aria-current="page">
							<a class="page-link">
								{{$i}}
								<span class="sr-only">(current)</span>
							</a>
						</li>
						@endif
					@endfor
					<li page="{{ $page + 1 }}"
						class="
							page-item 
							{{ $page == $totalPages ? 'disabled' : '' }} 
						" 
					>
						<a 
							class="page-link" 
							aria-disabled="{{ $page == $totalPages ? 'true' : 'false' }}" 
							href="{{ $page == $totalPages ? '' : '#' }}"
						>&raquo;</a>
					</li>
				</ul>
			</nav><label>Total: {{ $totalRows }}</label>
			<div class="input-group mb-3">
				<input 
					type="text" 
					class="form-control"  
					name="search" 
					value="{{ $search }}"
					placeholder="Buscar por producto">
				<div class="input-group-append">
					<button class="btn btn-sm btn-success">Buscar</button>
				</div>
			</div>
		</form>
		<div class="table-container2">
			<table 
				class="
					table 
					table-hover 
					table-borderer 
					table-stripped">
				<thead class="noselect">
					<tr>
						<th>
							<button 
								class="btn btn-sm btn-success" 
								refresh-action 
								refresh-for="#sales_form">
								<i class="fa fa-refresh"></i>
							</button>
						</th>
						<th>Productos</th>
						<th>Total</th>
						<th>Creado el</th>
						<th>Actualizado el</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($records as $record_idx => $record)
						<tr>
							<td>{{ $rowsPerPage * ($page - 1) + $record_idx + 1 }}</td>
							<td>
								@php
									$products_list = json_decode($record->json_products,true);  
								@endphp
								Código de venta: {{ $record->format_sale_code() }}
								<ul>
									@foreach($products_list AS &$product)
										@php
											$prod_purchase_price = null; 
											$prod_record = \App\Product::find($product['id']);
											if(isset($product['prod_purchase_price']) && \is_numeric($product['prod_purchase_price'])){
												$prod_purchase_price = $product['prod_purchase_price'];
											}else{
												if($prod_record){
													//$prod_last_acquisition = $prod_record->acquisitions_by_barcode->last();
													$prod_purchase_price = $prod_record->avg_price_from_acquisitions_discounting_stock();
												}	
											}
										@endphp
										<li>
											{{ $product['prod_name'] }}, 
											({{ \App\Helpers\SaleHelper::get_prod_type_fullname($product) }} <b>{{ $product['prod_price'] }}</b>{{ $prod_purchase_price ? ", ".round($prod_purchase_price,2) : '' }}), 
											cantidad: <b>{{ $product['prod_quantity'] ?? 1 }}{{ \App\Helpers\SaleHelper::get_prod_type_shortname($product) }}</b>
											<div>Total sin descuento: {{ sprintf('%0.2f',(float)$product['prod_quantity'] * (float)$product['prod_price'] * ( @$product['prod_by_weight'] == 'yes' ? 1/1000.0 : 1 )) }}</div>
											@if( !empty($product['discount_sale']) && $product['discount_sale'] == 'yes' )
											<div>
												<i><strong>Vendido con descuento por el total {{ sprintf('%0.2f',$product['discount_sale_price']) }}</strong></i>
											</div>
											@endif
										</li>
									@endforeach
								</ul>
							</td>
							<td>S/. {{ sprintf('%0.2f',$record->calc_total_values($products_list)) }}</td>
							<td>
								{{ 
									\Carbon\Carbon::parse($record->created_at)
									->setTimezone(env('APP_TIMEZONE'))
									->format('d/m/Y h:i:s a') 
								}}
							</td>
							<td>{{ \Carbon\Carbon::parse($record->updated_at)->setTimezone(env('APP_TIMEZONE'))->format('d/m/Y h:i:s a') }}</td>
							<td>
								<a href="{{ route('sales_show',$record->id) }}">
									<button class="mt-1 btn btn-sm btn-primary">
										<i class="fa fa-edit"></i>
									</button>
								</a>
								<button 
									class="mt-1 btn {{ isset($show_deleted) ? 'btn-warning' : 'btn-danger' }} btn-sm" 
									del-sale-action 
									record-code="{{ $record->format_sale_code() }}"
									record-id="{{ $record->id }}"  
									record-name="{{ $record->prod_name }}">
									<i class="fa fa-trash"></i>
								</button>
								@isset($record->deleted_at)
								<div>
									Eliminado el {{ $record->deleted_at->setTimeZone(env('APP_TIMEZONE'))->format('d/m/Y H:i:s a') }}
								</div>
								@endisset
								<form id="form_del_{{ $record->id }}" method="POST" action="{{ route('sales_delete',['id' => $record->id]) }}">
									@csrf
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
