@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>
			{{{ $record ? ( "Proveedor: " . $record->sup_name ) : 'Proveedor no encontrado' }}}
		</label>
	</div>
	<div class="card-body">
		<div class="container">
			@if(isset($record))
				@foreach ($errors->all() as $error)
				<div class="alert alert-danger" role="alert">
					<i class="fa fa-times close-alert"></i>
				{{ $error }}
				</div>
				@endforeach
				@if(isset($record))
					<a href="{{ route('product_suppliers') }}">
						Regresar a proveedores
					</a>
				@endif
				<form id="prod_form_update" method="POST" action="{{ route('product_suppliers_update',$record->id) }}">
					@include('product_suppliers.form')
				</form>
				<button class="btn btn-primary btn-sm btn-product-update">Modificar</button>
				<button 
					class="btn btn-danger btn-sm"
					del-action 
					record-id="{{ $record->id }}" 
					record-name="{{ $record->sup_name }}" 
					mod-name="product_supplier"

					>
					Eliminar
				</button>
				<form id="form_del_{{ $record->id }}" method="POST" action="{{ route('product_suppliers_delete',$record->id) }}">
					@csrf
				</form>
				<hr />
				<label class="required_asterisk">Campo obligatorio</label>
			@else
				<a href="{{ route('product_suppliers') }}">
					Regresar a proveedores
				</a>
			@endif
		</div>
	</div>
</div>
@endsection
