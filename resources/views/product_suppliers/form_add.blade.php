@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>Nuevo proveedor</label>
		<a href="{{ route('product_suppliers') }}">
			<button class="btn btn-primary btn-sm">Ir a proveedores</button>
		</a>
	</div>
	<div class="card-body">
		<div class="container">
			@foreach ($errors->all() as $error)
			<div class="alert alert-danger" role="alert">
				<i class="fa fa-times close-alert"></i>
			{{ $error }}
			</div>
			@endforeach
			<form method="POST" action="{{ route('product_suppliers_create') }}">
				@include('product_suppliers.form')
				<button class="btn btn-primary btn-sm" type="submit">Guardar</button>
			</form>
			<hr />
			<label class="required_asterisk">Campo obligatorio</label>
		</div>
	</div>
</div>
@endsection
