@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		@if($is_trashed)
		<label>
			{{ $record ? ( $deleted ? 'Registro de proveedor restaurado' : 'No se pudo restaurar el registro de proveedor' ) : 'El proveedor no existe' }}
		</label>
		@else
		<label>
			{{ $record ? ( $deleted ? 'Registro de proveedor eliminado' : 'No se pudo eliminar el registro de proveedor' ) : 'El proveedor no existe' }}
		</label>
		@endif
	</div>

	<div class="card-body">
		<div class="container">
			@if($record && $deleted)
			<label>Registro de proveedor {{ $is_trashed ? 'restaurado' : 'eliminado' }}: {{ $record->sup_name }}</label><br />
			@endif
			<a href="{{ route('product_suppliers') }}">
				<button class="btn btn-primary btn-sm">Regresar a proveedores de productos</button>
			</a>
		</div>
	</div>
</div>
@endsection
