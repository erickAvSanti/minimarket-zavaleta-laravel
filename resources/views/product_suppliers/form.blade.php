				<input type="hidden" name="from" value="ui">
				@csrf
				<div class="form-group row">
					<label 
						for="sup_name" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Proveedor</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="sup_name" 
							name="sup_name" 
							value="{{ isset($record) ? $record->sup_name : old('sup_name') }}"
							placeholder="El nombre del producto" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="document_number" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">#Documento</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="document_number" 
							name="document_number" 
							value="{{ isset($record) && !empty($record->document_number) ? $record->document_number : old('document_number') }}"
							placeholder="#Documento" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="document_type" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Tipo de Documento</label>
					<div class="col-sm-10">
				    <select 
							class=" 
								form-control 
								form-control-md" 
							name="document_type" 
				    	id="document_type">
				    	<option>SELECCIONAR</option>
				      <option value="DNI" {{ 
				      	( isset($record) && $record->document_type == 'DNI' ) || 
				      	old('document_type') == 'DNI' ? 'selected' : '' }} >DNI</option>
				      <option value="RUC" {{ 
				      	( isset($record) && $record->document_type == 'RUC' ) || 
				      	old('document_type') == 'RUC' ? 'selected' : '' }} >RUC</option>
				    </select>
				   </div>
			  </div>
				<div class="form-group row">
					<label 
						for="sup_desc" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md">Detalle adicional sobre el proveedor</label>
					<div class="col-sm-10">
						<textarea 
							class="
								form-control 
								form-control-md" 
							id="sup_desc" 
							name="sup_desc" 
							placeholder="Tipee algo aquí(opcional)" >{{ isset($record) ? $record->sup_desc : old('sup_desc') }}</textarea>
					</div>
				</div>