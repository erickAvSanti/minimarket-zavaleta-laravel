@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>Registro de adquisiciones</label>
	</div>
	<div class="card-body">
		<div class="container">
			@include('product_suppliers.preview_info')
			<hr />
			<manage-acquisitions-made-by-suppliers 
				v-bind:url_acquisitions="'{{ route( 'product_suppliers_acquisitions', $record->id ) }}'" 
				v-bind:current_supplier="{{ $record->toJson() }}"
			></manage-acquisitions-made-by-suppliers>
		</div>
	</div>
</div>
@endsection
