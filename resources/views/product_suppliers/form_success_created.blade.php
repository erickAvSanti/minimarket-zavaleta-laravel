@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>PROVEEDOR DE PRODUCTOS CREADO</label>
	</div>

	<div class="card-body">
		<div class="container">
			<label>Proveedor: {{ $record->sup_name }}</label><br />
			<a href="{{ route('product_suppliers_show',$record->id) }}">
				<button class="btn btn-primary btn-sm">Modificar Proveedor</button>
			</a>
			<a href="{{ route('product_suppliers') }}">
				<button class="btn btn-primary btn-sm">Ir a proveedores</button>
			</a>
		</div>
	</div>
</div>
@endsection
