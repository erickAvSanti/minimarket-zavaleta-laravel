<div>
				<ul>
					<li>
						<label><strong>Proveedor:</strong></label>
						<span>{{ $record->sup_name }}</span>
					</li>
					<li>
						<label><strong>Tipo de documento:</strong></label>
						<span>{{ $record->document_type ? $record->document_type : '-' }}</span>
					</li>
					<li>
						<label><strong>Documento:</strong></label>
						<span>{{ $record->document_number ? $record->document_number : '-' }}</span>
					</li>
					<li>
						<label><strong>Descripción:</strong></label>
						<span>{{ $record->sup_desc ? $record->sup_desc : '-' }}</span>
					</li>
				</ul>
			</div>
			<a href="{{ route('product_suppliers_show',$record->id) }}">
				<button class="btn btn-primary btn-sm">Editar Proveedor</button>
			</a>
			<a href="{{ route('product_suppliers') }}">
				<button class="btn btn-primary btn-sm">Ir a proveedores</button>
			</a>