@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>PROVEEDOR DE PRODUCTOS ACTUALIZADO</label>
	</div>

	<div class="card-body">
		<div class="container">
			@include('product_suppliers.preview_info')
		</div>
	</div>
</div>
@endsection
