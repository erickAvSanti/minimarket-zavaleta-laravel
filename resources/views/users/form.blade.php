				<input type="hidden" name="from" value="ui">
				@csrf
				<div class="form-group row">
					<label 
						for="name" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Usuario</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="name" 
							name="name" 
							value="{{ isset($record) ? $record->name : old('name') }}"
							placeholder="Usuario" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="email" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Email</label>
					<div class="col-sm-10">
						<input 
							type="text" 
							class="
								form-control 
								form-control-md" 
							id="email" 
							name="email" 
							value="{{ isset($record) ? $record->email : old('email') }}"
							placeholder="Email" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="user_password" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Contraseña</label>
					<div class="col-sm-10">
						<input 
							type="password" 
							class="
								form-control 
								form-control-md" 
							id="password" 
							name="password" 
							value="{{ old('password') }}"
							placeholder="Contraseña" />
					</div>
				</div>
				<div class="form-group row">
					<label 
						for="password_confirmation" 
						class="
							col-sm-2 
							col-form-label 
							col-form-label-md required">Confirmar contraseña</label>
					<div class="col-sm-10">
						<input 
							type="password" 
							class="
								form-control 
								form-control-md" 
							id="password_confirmation" 
							name="password_confirmation" 
							value="{{ old('password_confirmation') }}"
							placeholder="Contraseña" />
					</div>
				</div>
				<div class="form-group row">
					<div class="form-check">
					  <input 
					  	class="form-check-input" 
					  	type="checkbox" 
							value="1" 
							name="is_admin"
							{{ (isset($record) && $record->is_admin == 1) || old('is_admin') == 1 ? 'checked' : '' }}
					  	id="is_admin">
					  <label 
					  	class="form-check-label" 
					  	for="is_admin">
					    Es administrador?
					  </label>
					</div>
				</div>