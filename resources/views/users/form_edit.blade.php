@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>
			{{{ $record ? ( "Usuario: " . $record->name ) : 'Usuario no encontrado' }}}
		</label>
	</div>
	<div class="card-body">
		<div class="container">
			@if(isset($record))
				@foreach ($errors->all() as $error)
				<div class="alert alert-danger" role="alert">
					<i class="fa fa-times close-alert"></i>
				{{ $error }}
				</div>
				@endforeach
				@if(isset($record))
					<a href="{{ route('users') }}">
						Regresar a usuarios
					</a>
				@endif
				<form 
					id="form_update" 
					method="POST" 
					action="{{ route('users_update',$record->id) }}">
					@include('users.form')
				</form>
				<button 
					class="
						btn 
						btn-primary 
						btn-sm 
						btn-user-update">Modificar</button>
				<button 
					class="btn btn-danger btn-sm"
					del-user-action 
					record-id="{{ $record->id }}" 
					record-name="{{ $record->name }}" 
					form-del="#form_del_{{ $record->id }}"
					>
					Eliminar
				</button>
				<form 
					id="form_del_{{ $record->id }}" 
					method="POST" 
					action="{{ route('users_delete',$record->id) }}">
					@csrf
				</form>
				<hr />
				<label class="required_asterisk">Campo obligatorio</label>
			@else
				<a href="{{ route('users') }}">
					Regresar a usuarios
				</a>
			@endif
		</div>
	</div>
</div>
@endsection
