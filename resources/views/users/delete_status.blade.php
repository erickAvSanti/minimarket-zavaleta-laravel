@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		@if($is_trashed)
		<label>
			{{ $record ? ( $deleted ? 'Registro de usuario restaurado' : 'No se pudo restaurar el registro de usuario' ) : 'El usuario no existe' }}
		</label>
		@else
		<label>
			{{ $record ? ( $deleted ? 'Registro de usuario eliminado' : 'No se pudo eliminar el registro de usuario' ) : 'El usuario no existe' }}
		</label>
		@endif
	</div>

	<div class="card-body">
		<div class="container">
			@if($record && $deleted)
			<label>Registro de usuario {{ $is_trashed ? 'restaurado' : 'eliminado' }}: {{ $record->name }}</label><br />
			@endif
			<a href="{{ route('users') }}">
				<button class="btn btn-primary btn-sm">Regresar a usuarios</button>
			</a>
		</div>
	</div>
</div>
@endsection
