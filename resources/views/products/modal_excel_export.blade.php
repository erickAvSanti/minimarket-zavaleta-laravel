<div 
  class="modal" 
  id="modal_excel_export" 
  tabindex="-1" 
  role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Reportes excel</h5>
        <button 
          type="button" 
          class="close" 
          data-dismiss="modal" 
          aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <a href="{{ route('products_excel_export_all') }}" target="_blank">
          <button class="btn btn-sm btn-success">
            <i class="fa fa-file-excel-o"></i> Exportar todo
          </button>
        </a>
        <button class="btn btn-sm btn-success" id="excel_export_current_filter">
          <i class="fa fa-file-excel-o"></i> Exportar filtro actual
        </button>
      </div>
      <div class="modal-footer">
        <button 
          type="button" 
          class="btn btn-secondary" 
          data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>