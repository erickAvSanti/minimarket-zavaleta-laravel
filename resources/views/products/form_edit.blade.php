@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>
			{{{ $record ? ( "Producto: " . $record->prod_name ) : 'Producto no encontrado' }}}
		</label>
	</div>
	<div class="card-body">
		<div class="container">
			@if(isset($record))
				@foreach ($errors->all() as $error)
				<div class="alert alert-danger" role="alert">
					<i class="fa fa-times close-alert"></i>
				{{ $error }}
				</div>
				@endforeach
				@if(isset($record))
					<a href="{{ route('products') }}">
						Regresar a productos
					</a>
				@endif
				<form id="prod_form_update" method="POST" action="{{ route('products_update',$record->id) }}">
					@include('products.form')
				</form>
				<button class="btn btn-primary btn-sm btn-product-update">Modificar</button>
				<button 
					class="btn btn-danger btn-sm" 
					mod-name="product"
					del-action 
					record-id="{{ $record->id }}" 
					record-name="{{ $record->prod_name }}" 

					>
					Eliminar
				</button>
				<form id="form_del_{{ $record->id }}" method="POST" action="{{ route('products_delete',$record->id) }}">
					@csrf
				</form>
				<hr />
				<label class="required_asterisk">Campo obligatorio</label>
			@else
				<a href="{{ route('products') }}">
					Regresar a productos
				</a>
			@endif
		</div>
	</div>
</div>
@endsection
