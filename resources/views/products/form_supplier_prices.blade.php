@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>PRECIO DE PROVEEDORES PARA ESTE PRODUCTO</label>
	</div>

	<div class="card-body">
		<div class="container">
			@if(isset($record))
			<div class="row">
				<div class="col-lg-6">
					@include('products.preview_info')
				</div>
				<div class="col-lg-6">
					@include('products.show_list_suppliers_for_this_product')
				</div>
			</div>
			<div>
				@if(count($suppliers_with_prices)>0)
					<form id="form_set_supplier_prices" method="POST" action="{{ route('product_suppliers_prices_update',$record->id) }}">
						@csrf
						@foreach($suppliers_with_prices as $reg)
							<div class="form-group row">
								<label 
									for="sup_id_{{ $reg['supplier']->id }}" 
									class="
										col-sm-2 
										col-form-label 
										col-form-label-md">{{ $reg['supplier']->sup_name }}</label>
								<div class="col-sm-10">
									<input 
										type="text" 
										class="
											form-control 
											form-control-md" 
										id="sup_id_{{ $reg['supplier']->id }}" 
										name="sup_id_{{ $reg['supplier']->id }}" 
										value="{{ $reg['price'] }}"
										placeholder="El precio del proveedor para este producto" />
								</div>
							</div>
						@endforeach
					</form>
				@else
					<h6>No existen proveedores registrados. Intente registrar al menos uno para asignar el precio de al menos un proveedor</h6>
				@endif
			</div>
			<button class="btn btn-primary btn-sm" id="btn_suppliers_prices">Guardar precios</button>
			@else
			<span>Producto no encontrado</span>
			@endif
			<a href="{{ route('products') }}">
				<button class="btn btn-primary btn-sm">Regresar a productos</button>
			</a>
		</div>
	</div>
</div>
@endsection
