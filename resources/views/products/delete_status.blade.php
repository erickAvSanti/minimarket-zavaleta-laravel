@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		@if($is_trashed)
		<label>
			{{ $record ? ( $deleted ? 'Registro de producto restaurado' : 'No se pudo restaurar el registro de producto' ) : 'El producto no existe' }}
		</label>
		@else
		<label>
			{{ $record ? ( $deleted ? 'Registro de producto eliminado' : 'No se pudo eliminar el registro de producto' ) : 'El producto no existe' }}
		</label>
		@endif
	</div>

	<div class="card-body">
		<div class="container">
			@if($record && $deleted)
			<label>Registro de producto {{ $is_trashed ? 'restaurado' : 'eliminado' }}: {{ $record->prod_name }}</label><br />
			@endif
			<a href="{{ route('products') }}">
				<button class="btn btn-primary btn-sm">Regresar a productos</button>
			</a>
		</div>
	</div>
</div>
@endsection
