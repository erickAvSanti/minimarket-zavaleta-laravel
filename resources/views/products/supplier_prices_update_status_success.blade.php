@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>
			Precios de proveedores actualizados para este producto
		</label>
	</div>

	<div class="card-body">
		<div class="container">
			<label>Producto: {{ $record->prod_name }}</label>
			<div>
				<ul>
					<li>
						<label><strong>Producto:</strong></label>
						<span>{{ $record->prod_name }}</span>
					</li>
					<li>
						<label><strong>Código de Barra:</strong></label>
						<span>{{ $record->prod_barcode }}</span>
					</li>
					<li>
						<label><strong>Precio de venta:</strong></label>
						<span>{{ $record->prod_price }}</span>
					</li>
					<li>
						<label><strong>Descripción:</strong></label><br />
						<div>{{ $record->prod_desc ? $record->prod_desc : '-' }}</div>
					</li>
				</ul>
			</div>
			<a href="{{ route('products_show',$record->id) }}" >
				<button class="btn btn-primary btn-sm mb-1">Editar Producto</button>
			</a>
			<a href="{{ route('products') }}" >
				<button class="btn btn-primary btn-sm mb-1">Ir a Producto</button>
			</a>
			<table class="max600 table table-hover table-borderer table-stripped">
				<thead>
					<tr>
						<th>Proveedor</th>
						<th>Precio de compra</th>
						<th>Precio de venta</th>
						<th>%</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($suppliers_prices as $reg)
					<tr>
						<td>
							{{ $reg['supplier']->sup_name }}
						</td>
						<td>
							{{ $reg['supplier_price'] }}
						</td>
						<td>
							{{ $record->prod_price }}
						</td>
						<td>
							{{ $reg['supplier_price'] > 0 ? "( $record->prod_price / {$reg['supplier_price']} ) = " . round( ((float)$record->prod_price) / $reg['supplier_price'], 2 ) . '%' : '-' }}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			<a href="{{ route('product_suppliers_prices_view',$record->id) }}" >
				<button class="btn btn-primary btn-sm mb-1">Editar precios de proveedores</button>
			</a>
		</div>
	</div>
</div>
@endsection
