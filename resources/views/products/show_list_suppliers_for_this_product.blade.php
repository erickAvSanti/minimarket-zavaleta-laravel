					<h5>Proveedores</h5>
					@if(count($suppliers)>0)
						<ul>
						@foreach($suppliers as $supplier)
							<li class="mb-1"> 
								<a href="{{ route('product_show_prices_by_supplier',['product_id' => $record->id, 'supplier_id' => $supplier->id]) }}">
									<button class="btn btn-outline-primary btn-sm">
										Ver registros
									</button>
								</a>
								<span>{{ $supplier->sup_name }}</span>
							</li>
						@endforeach
						</ul>
					@endif