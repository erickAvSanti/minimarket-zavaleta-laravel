@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		<label>Proveedores {{ isset($show_deleted) ? 'eliminados' : '' }}</label>
		<a href="{{ route('product_suppliers_new') }}">
			<button class="btn btn-primary btn-sm">Agregar Proveedor</button>
		</a>
	</div>

	<div class="card-body">
		<form id="product_suppliers_form" method="GET" action="">
			<input type="hidden" name="order_by" value="{{ $order_by ?? '' }}">
			<input type="hidden" name="order" value="{{ $order ?? '' }}">
			<div class="input-group mb-3">
			  <div class="input-group-prepend">
			    <span class="input-group-text" id="basic-prod-search">
			    	<i class="fa fa-search"></i>
			    </span>
			  </div>
			  <input 
			  	name="query"
			  	type="text" 
			  	class="form-control" 
			  	name="query"
			  	value="{{ $query ?? ''}}"
			  	placeholder="Búsqueda" 
			  	aria-label="Búsqueda de proveedores" 
			  	aria-describedby="basic-prod-search" />
				  <div class="input-group-append">
				    <button class="btn btn-success" id="basic-prov-search">
				    	Buscar
				    </button>
				  </div>
			</div>
			<div class="form-check noselect">
			  <input 
			  	class="form-check-input" 
			  	type="checkbox" 
			  	value="1" 
			  	name="show_deleted" 
			  	{{ isset($show_deleted) ? 'checked' : '' }} 
			  	id="show_deleted">
			  <label class="form-check-label" for="show_deleted">
			    Buscar en eliminados
			  </label>
			</div>
		</form>
		<div class="table-container2">
			<table 
				class="
					table 
					table-hover 
					table-borderer 
					table-stripped">
				<thead class="noselect">
					<tr>
						<th>#</th>
						<th 
							form_tag_id="#product_suppliers_form" 
							order_by="sup_name" 
							order="{{ $order_by == 'sup_name' ? $order : '' }}">Proveedor</th>
						<th 
							form_tag_id="#product_suppliers_form" 
							order_by="document_type" 
							order="{{ $order_by == 'document_type' ? $order : '' }}">Tipo de documento</th>
						<th 
							form_tag_id="#product_suppliers_form" 
							order_by="document_number" 
							order="{{ $order_by == 'document_number' ? $order : '' }}">Documento</th>
						<th>Acción</th>
					</tr>
				</thead>
				<tbody>
					@foreach($records as $idx => &$record)
						<tr>
							<td>{{ $idx + 1 }}</td>
							<td>
								<span>{{ $record->sup_name }}</span>
								<div>
									<a href="{{ route('product_suppliers_acquisitions',$record->id) }}">Adquisiciones</a>
								</div>
							</td>
							<td>{{ $record->document_type }}</td>
							<td>{{ $record->document_number }}</td>
							<td>
								<a href="{{ route('product_suppliers_show',['id' => $record->id]) }}">
									<button class="btn btn-primary btn-sm">
										Ver
									</button>
								</a>
								<button 
									class="btn {{ isset($show_deleted) ? 'btn-warning' : 'btn-danger' }} btn-sm" 
									del-action 
									mod-name="product_supplier" 
									record-id="{{ $record->id }}"  
									record-name="{{ $record->sup_name }}">
									{{ isset($show_deleted) ? 'Deshacer eliminación' : 'Eliminar' }}
								</button>
								<form 
									id="form_del_{{ $record->id }}" 
									method="POST" 
									action="{{ route('product_suppliers_delete',['id' => $record->id]) }}">
									@csrf
								</form>
							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
@endsection
