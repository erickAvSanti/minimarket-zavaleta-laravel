@extends('errors::minimal')

@section('title', __('Espacio no encontrado'))
@section('code', '404')
@section('message', __('Not Found'))
