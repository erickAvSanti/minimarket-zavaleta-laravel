@extends('layouts.app')

@section('content')
<div class="card">
	<div class="card-header">
		@if($is_trashed)
		<label>
			{{ $record ? ( $deleted ? 'Registro de venta restaurado' : 'No se pudo restaurar el registro de venta' ) : 'La venta no existe' }}
		</label>
		@else
		<label>
			{{ $record ? ( $deleted ? 'Registro de venta eliminado' : 'No se pudo eliminar el registro de venta' ) : 'La venta no existe' }}
		</label>
		@endif
	</div>

	<div class="card-body">
		<div class="container">
			@if($record && $deleted)
			<label>Registro de venta {{ $is_trashed ? 'restaurado' : 'eliminado' }}: {{ $record->format_sale_code() }}</label><br />
			@endif
			<a href="{{ route('sales') }}">
				<button class="btn btn-primary btn-sm">Regresar a ventas</button>
			</a>
		</div>
	</div>
</div>
@endsection
