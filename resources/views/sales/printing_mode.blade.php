<style type="text/css">
	@page { size: auto;  margin: 0mm; }
	*{
		font-family: Arial, Helvetica, sans-serif;
	}
	table{
		font-size: 11px;
	}
	.center{
		text-align: center;
	}
	.hh{
		margin-top: 5px;
		margin-bottom: 5px;
	}
</style>
@php
	$products_arr = json_decode($record->json_products,true);
@endphp
<h3 class="center hh">Minimarket Zavaleta</h3>
<h5 class="center hh">Registro de pedidos</h5>
<h5 class="center hh">{{ $record->format_sale_code() }}</h5>
@if(is_array($products_arr))
	<table>
		<tr>
			<th>#</th>
			<th style="text-align: left;">Producto</th>
			<th style="text-align: left;">Cantidad</th>
			<th style="text-align: left;">Prec.<br />Unidad/<br />kilo/<br />Litro.</th>
			<th style="text-align: left;">Prec.<br />Total</th>
		</tr>

		@php
		$total_price = 0.0;
		@endphp
		@foreach($products_arr AS $product_index => $product)
		@php
			if(empty(@$product['discount_sale']) || @$product['discount_sale'] == 'no')
			{
				if($product['prod_by_weight'] == 'yes'){
					$total_by_product = @$product['prod_quantity'] * ( @$product['prod_price'] / 1000.0 );
				}else{
					$total_by_product = @$product['prod_quantity'] * @$product['prod_price'];
				}
			}else{
				$total_by_product = (float)@$product['discount_sale_price'];
			}
			$total_price += $total_by_product;
		@endphp
		<tr>
			<td>{{ str_pad($product_index + 1, 2, '0', STR_PAD_LEFT) }})</td>
			<td>{{ $product['prod_name'] }}.</td>
			<td>{{ $product['prod_quantity'] }}{{ $product['prod_by_weight'] == 'yes' ? ( $product['prod_weight_type'] ) : ' u' }}</td>
			<td>S/. {{ $product['prod_price'] }}</td>
			<td>S/. {{ sprintf("%.2f",$total_by_product) }}</td>
		</tr>
		@endforeach
	</table>
	<div style="text-align: center;">
		<h5>Total: S/. {{ sprintf("%.2f",$total_price) }}</h5>
	</div>
@endif