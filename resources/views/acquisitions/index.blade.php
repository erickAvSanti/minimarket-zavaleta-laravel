@extends('layouts.app')

@section('content')
<manage-acquisitions 
	v-bind:server_url="'{{ route('acquisitions') }}'" 
	v-bind:server_supplier_url="'{{ route('product_suppliers') }}'" 
	v-bind:server_measurement_unit_url="'{{ route('measurement_units') }}'" 

	></manage-acquisitions>
@endsection
