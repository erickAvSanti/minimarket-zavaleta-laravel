/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap')
window.Swal = require('sweetalert2')

window.Vue = require('vue')

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
Vue.component('manage-product-items-on-sale', require('./components/ManageProductItemsOnSale.vue').default);
Vue.component('manage-product-prices-by-supplier', require('./components/ManageProductPricesBySupplier.vue').default);
Vue.component('manage-acquisitions-made-by-suppliers', require('./components/ManageAcquisitionsMadeBySuppliers.vue').default);
Vue.component('manage-acquisitions', require('./components/ManageAcquisitions.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
	el: '#app',
});
require('./sidebar-nav')
require('./alerts')
require('./products')
require('./sales')
require('./users')

window.axiosErrorToString = function(obj){
	let str = `
	<h4>Algunos de los datos son inválidos</h4>	
	`
	if( obj.errors ){
		for(const idx in obj.errors){
			const arr = obj.errors[idx]
			str += `<ul>`
			for(const msg of arr){
				str += `<li>${msg}</li>`
			}
			str += `</ul>`
		}
	}
	return str
}

jQuery('button[refresh-action]').click(function(evt){
	const obj = jQuery(this)
	const refresh_for = obj.attr('refresh-for')
	jQuery(refresh_for).submit()
})
jQuery('#main-menu').click(function(evt){
	jQuery('#left-side').toggleClass('menu-open')
	jQuery('#right-side').toggleClass('menu-open')
})

window.formToStringUri = function(form){
	const props = []
	form.find('input[type=hidden]').each((idx,val)=>{
		const tag = jQuery(val)
		props.push({
			name:tag.attr('name'),
			value:tag.val(),
		})
	})
	form.find('input[type=text]').each((idx,val)=>{
		const tag = jQuery(val)
		props.push({
			name:tag.attr('name'),
			value:tag.val(),
		})
	})
	form.find('input[type=radio]').each((idx,val)=>{
		const tag = jQuery(val)
		if(tag.is(':checked')){
			props.push({
				name:tag.attr('name'),
				value:tag.val(),
			})
		}
	})
	form.find('input[type=checkbox]').each((idx,val)=>{
		const tag = jQuery(val)
		if(tag.is(':checked')){
			props.push({
				name:tag.attr('name'),
				value:tag.val(),
			})
		}
	})
	console.log(props)
	const tmp_arr = []
	props.forEach((prop)=>{
		tmp_arr.push(`${prop.name}=${prop.value}`)
	})
	return tmp_arr.join('&')
}