window._ = require('lodash');

/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
	window.Popper = require('popper.js').default;
	window.$ = window.jQuery = require('jquery');

	require('bootstrap');
} catch (e) {}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */

window.axios = require('axios');

window.axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo';

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     forceTLS: true
// });

import Vue from 'vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'

// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
	faSearch, 
	faChevronLeft,
	faChevronRight,
	faTimes,
	faSync,
	faPlus,
	faTrash,
	faEdit,
	faFileExcel,
} from '@fortawesome/free-solid-svg-icons'

import {
	faCircle as farFaCircle,
	faCheckCircle as farFaCheckCircle,
	faFileExcel as farFaFileExcel,
	faFilePdf as farFaFilePdf, 
	faCopy as farFaCopy,
	faHandPointLeft,
	faSquare,
} from '@fortawesome/free-regular-svg-icons'


import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(
	faSearch,
	faChevronLeft,
	faChevronRight,
	faTimes,
	faSync,
	faPlus,
	faTrash,
	faEdit,
	farFaFileExcel,
)

Vue.component('fai', FontAwesomeIcon)

import axios from 'axios'
import VueAxios from 'vue-axios'
 
Vue.use(VueAxios, axios)

import VueMoment from 'vue-moment'
import moment from 'moment-timezone' 
 
Vue.use(VueMoment, {
    moment,
})

moment.tz.setDefault('America/Lima')

import datePicker from 'vue-bootstrap-datetimepicker';
import 'pc-bootstrap4-datetimepicker/build/css/bootstrap-datetimepicker.css';
Vue.component('date-picker', datePicker)