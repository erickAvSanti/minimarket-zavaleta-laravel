jQuery(window).ready(function(){
	
	jQuery("button[del-sale-action]").click(function(evt){
		const obj = jQuery(this)
		const id = obj.attr('record-id')
		const name = obj.attr('record-name')
		const code = obj.attr('record-code')

		const already_deleted = obj.hasClass('btn-warning')
		Swal.fire({
		  html: `Seguro de ${already_deleted ? 'deshacer la eliminación de' : 'eliminar'} la venta <b>${code}</b> ?`,
		  text: "",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si',
		  cancelButtonText: 'Cancelar acción'
		}).then((result) => {
		  if (result.value) {
		  	jQuery(`#form_del_${id}`).submit()
		  }
		})
	})

	jQuery('.sales_pagination li:not(.disabled)').click(function(evt){
		const page = jQuery(this).attr('page')
		if(/\d+/.test(page)){
			jQuery('input[name=page]').val(page)
			jQuery('#sales_form').submit()
		}
	})

	jQuery(".sales_rows_x_page a").click(function(evt){
		jQuery('input[name=rows_x_page]').val(jQuery(this).attr('rows_x_page'))
		jQuery('#sales_form').submit()
	})

})



jQuery(window).ready(function(){
	jQuery('#excel_report_from_date').datepicker({
		format: 'dd/mm/yyyy',
	})
	jQuery('#excel_report_to_date').datepicker({
		format: 'dd/mm/yyyy',
	})
	jQuery('#excel_export_from_dates').click(function(evt){
		const from_date = jQuery('#excel_report_from_date input').val()
		const to_date = jQuery('#excel_report_to_date input').val()

		const date_regex = /^\d{2}\/\d{2}\/\d{4}$/
		if(
			date_regex.test(from_date) && 
			date_regex.test(to_date)
		){
			window.open(`/ventas/reportes-excel?by=specific_dates&from_date=${from_date}&to_date=${to_date}`, "_blank");
		}else{
			Swal.fire({title:'Indique el rango de fechas',icon:'info'})
		}
	})
	jQuery('#sales_excel_export_from_ids').click(function(evt){
		const excel_export_from_id = jQuery('#excel_export_from_id').val()
		const excel_export_to_id = jQuery('#excel_export_to_id').val()

		if(
			/\d+/.test(excel_export_from_id) && 
			/\d+/.test(excel_export_to_id)
		){
			window.open(`/ventas/reportes-excel?by=specific_ids&from_id=${excel_export_from_id}&to_id=${excel_export_to_id}`, "_blank");
		}else{
			Swal.fire({title:'Indique el rango de ID de tickets',icon:'info'})
		}
	})


	jQuery('#created_at_from').datepicker({
		format: 'dd/mm/yyyy',
	})
	jQuery('#created_at_to').datepicker({
		format: 'dd/mm/yyyy',
	})

	jQuery('#created_at_from').parent().find('.btn-danger').click(function(){
		jQuery('input[name=created_at_from]').val('')
	})

	jQuery('#created_at_to').parent().find('.btn-danger').click(function(){
		jQuery('input[name=created_at_to]').val('')
	})


	jQuery('input[name=sale_price_from]').parent().find('.btn-danger').click(function(){
		jQuery('input[name=sale_price_from]').val('')
	})

	jQuery('input[name=sale_price_to]').parent().find('.btn-danger').click(function(){
		jQuery('input[name=sale_price_to]').val('')
	})


})