jQuery(window).ready(function(){
	jQuery("button[del-action]").click(function(evt){
		const obj = jQuery(this)
		const id = obj.attr('record-id')
		const name = obj.attr('record-name')
		let mod_name = obj.attr('mod-name')
		if(mod_name == 'product'){
			mod_name = 'el producto'
		}else if(mod_name == 'product_supplier'){
			mod_name = 'el proveedor de productos'
		}else{
			return
		}
		const already_deleted = obj.hasClass('btn-warning')
		Swal.fire({
		  title: `Seguro de ${already_deleted ? 'deshacer la eliminación de' : 'eliminar'} ${mod_name} '${name}'?`,
		  text: "",
		  icon: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si',
		  cancelButtonText: 'Cancelar acción'
		}).then((result) => {
		  if (result.value) {
		  	jQuery(`#form_del_${id}`).submit()
		  }
		})
	})

	jQuery('th[order_by]').click(function(evt){
		const obj = jQuery(this)
		const form_tag_id = obj.attr('form_tag_id')
		let order = ''
		let order_by = ''
		if(obj.attr('order')==''){
			order = 'desc'
		}else if(obj.attr('order')=='asc'){
			order = 'desc'
		}else{
			order = 'asc'
		}
		obj.attr('order',order)
		const form = jQuery(form_tag_id)
		form.find('[name=order_by]').attr('value',obj.attr('order_by'))
		form.find('[name=order]').attr('value',order)
		form.submit()
	})

	jQuery('.btn-product-update').click(function(evt){
		jQuery('#prod_form_update').submit()
	})

	jQuery('#btn_suppliers_prices').click(function(evt){
		jQuery('#form_set_supplier_prices').submit()
	});

	jQuery('#prod_form_create input').keydown(function(evt){
		var keyCode = event.keyCode ? event.keyCode : ( event.which ? event.which : event.charCode )
    if( keyCode == 13 ) {
    	evt.stopPropagation()
    	evt.preventDefault()
  	}
	})

	jQuery('.products_pagination li:not(.disabled)').click(function(evt){
		const page = jQuery(this).attr('page')
		if(/\d+/.test(page)){
			jQuery('input[name=page]').val(page)
			jQuery('#products_form').submit()
		}
	})

	jQuery(".products_rows_x_page a").click(function(evt){
		jQuery('input[name=rows_x_page]').val(jQuery(this).attr('rows_x_page'))
		jQuery('#products_form').submit()
	})

	jQuery('#excel_export_current_filter').click(function(evt){
		const url = `${window.location.origin}${window.location.pathname}/exportar-excel/by?${formToStringUri(jQuery('#products_form'))}`
		console.log(url)
		window.open(url)
	})
})